﻿const path = require('path');
const webpack = require('webpack');
//const glob = require('glob');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const PurifyCSSPlugin = require('purifycss-webpack');
const glob = require('glob-all');

module.exports = (env) => {
    const extractCSS = new ExtractTextPlugin('vendor.css');
    const isDevBuild = !(env && env.prod);

    const vendorConfig = {
        module: {
            rules: [
                { test: /\.(jpg|png|woff|woff2|eot|ttf|svg)(\?|$)/, use: 'url-loader?limit=100000' },
                { test: /\.css(\?|$)/, use: extractCSS.extract({ use: isDevBuild ? 'css-loader' : 'css-loader?minimize' }) },
                {
                    test: require.resolve('jquery'),
                    use: [{
                        loader: 'expose-loader',
                        options: 'jQuery'
                    }, {
                        loader: 'expose-loader',
                        options: '$'
                    }]
                }
            ]
        },

        entry: {
            vendor: [
                'jquery',
                'bootstrap/dist/js/bootstrap',
                './Content/js/grayscale.js',
                'bootstrap/dist/css/bootstrap.css',
                './Content/css/grayscale.css',
            ]
        },

        output: {
            path: path.join(__dirname, 'wwwroot', 'dist'),
            filename: "[name].bundle.js",
        },
        plugins: [
            extractCSS,
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery"
            }),
            //Purify Layout web template
            new PurifyCSSPlugin({
                paths: glob.sync([
                    path.join(__dirname, 'Views/Home/Index.cshtml'),
                    path.join(__dirname, 'Views/Shared/_LayoutWeb.cshtml')
                ]),
                purifyOptions: { info: true, minify: true }
            }),
            new CopyWebpackPlugin([
                { from: './Content/sw-PWA.NetCore.js', to: path.join(__dirname, 'wwwroot') },
                { from: './Content/manifest.json', to: path.join(__dirname, 'wwwroot') },
            ]),
            new webpack.optimize.UglifyJsPlugin({ minimize: true })
        ]
    }

    const cssConfig = {



    }

    const botConfig = {

        entry: {
            app: ['./Content/js/botframeworkscript.js'],

        },
        output: {
            path: path.join(__dirname, 'wwwroot', 'dist'),
            filename: "bot.bundle.js",
            libraryTarget: 'var',
            library: 'ui'
        },
    };

    const appConfig = {

        entry: {
            app: ['./Content/js/init.js', './Content/js/firebase.js'
                , './Content/js/site.js'],
        },
        output: {
            path: path.join(__dirname, 'wwwroot', 'dist'),
            filename: "[name].bundle.js",
            libraryTarget: 'var',
            library: 'ui'
        },
    };

    return [vendorConfig, appConfig, botConfig];
};
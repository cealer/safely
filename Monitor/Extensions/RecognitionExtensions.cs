﻿using FCM.Net;
using Microsoft.EntityFrameworkCore;
using Microsoft.ProjectOxford.Face;
using Microsoft.ProjectOxford.Face.Contract;
using Microsoft.ProjectOxford.Vision;
using Microsoft.ProjectOxford.Vision.Contract;
using Monitor.Data;
using Monitor.Model;
using Monitor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Monitor.Extensions
{
    public class RecognitionExtensions
    {
        private readonly ApplicationDbContext _context;
        private Generator _generate;

        const string subscriptionKey = "3cdd72f7d08b4deeb10532e59fb2193f";

        public async Task<List<string>> GetPerson(string groupId, string PersonId)
        {
            List<string> photosId = new List<string>();
            var faceServiceClient = new FaceServiceClient(subscriptionKey);
            Guid guid = new Guid(PersonId);
            Person p = await faceServiceClient.GetPersonAsync(groupId, guid);
            foreach (var item in p.PersistedFaceIds)
            {
                photosId.Add(item.ToString());
            }
            return photosId;
        }

        public async Task<string> DeletePhotoPersonAsync(PhotoPerson photoPerson, string personGroupId, ApplicationUser User)
        {
            try
            {
                var faceServiceClient = new FaceServiceClient(subscriptionKey);

                Guid idPerson = new Guid(photoPerson.personIdApi);
                Guid PersistedFace = new Guid(photoPerson.persistedFaceIds);
                // Detect faces in the image and add
                await faceServiceClient.DeletePersonFaceAsync(
                     personGroupId.ToLower(), idPerson, PersistedFace);

                await faceServiceClient.TrainPersonGroupAsync(personGroupId.ToLower());
                
                return "1";
            }
            catch (Exception)
            {

                return "-1";
            }

        }

        public async Task<PhotoPerson> AddPhotoPersonAsync(string imageUrl, Model.People people, string personGroupId, ApplicationUser User)
        {
            var faceServiceClient = new FaceServiceClient(subscriptionKey);

            Guid idPerson = new Guid(people.PerPath);

            // Detect faces in the image and add
            var r = await faceServiceClient.AddPersonFaceAsync(
                 personGroupId.ToLower(), idPerson, imageUrl);

            await faceServiceClient.TrainPersonGroupAsync(personGroupId.ToLower());

            var id = await _generate.GenerateIdAsync("PHO", "Register.PhotoPerson");

            var photo = new PhotoPerson
            {
                DateRegister = DateTimeExtension.GetUserDateTime(User.TimeZone),
                PhoId = id,
                Enabled = true,
                UserId = User.Id,
                UrlImage = imageUrl,
                persistedFaceIds = r.PersistedFaceId.ToString(),
                personIdApi = people.PerPath,
                PerId = people.PerId
            };
            return photo;
        }


        public async Task<PhotoPerson> CreatePersonAsync(string imageUrl, Model.People people, string personGroupId, ApplicationUser User)
        {
            var faceServiceClient = new FaceServiceClient(subscriptionKey);

            // Define Cesar
            CreatePersonResult person = await faceServiceClient.CreatePersonAsync(
                // Id of the person group that the person belonged to
                personGroupId.ToLower(),
                // Name of the person
                $"{people.FullName}-{people.IdentificationDocument}"
            );

            // Detect faces in the image and add
            var r = await faceServiceClient.AddPersonFaceAsync(
                 personGroupId.ToLower(), person.PersonId, imageUrl);

            await faceServiceClient.TrainPersonGroupAsync(personGroupId.ToLower());

            var id = await _generate.GenerateIdAsync("PHO", "Register.PhotoPerson");

            var photo = new PhotoPerson
            {
                DateRegister = DateTimeExtension.GetUserDateTime(User.TimeZone),
                PhoId = id,
                Enabled = true,
                UserId = User.Id,
                UrlImage = imageUrl,
                persistedFaceIds = r.PersistedFaceId.ToString(),
                personIdApi = person.PersonId.ToString(),
                PerId = people.PerId
            };
            return photo;
        }

        public RecognitionExtensions()
        {

        }

        public RecognitionExtensions(ApplicationDbContext context)
        {
            _context = context;
            _generate = new Generator(context);
        }

        public async Task<string> IdentifyPerson(string personPhoto, string personGroupId)
        {
            var faceServiceClient = new FaceServiceClient(subscriptionKey);

            var faces = await faceServiceClient.DetectAsync(personPhoto);
            var faceIds = faces.Select(face => face.FaceId).ToArray();

            var results = await faceServiceClient.IdentifyAsync(personGroupId.ToLower(), faceIds);

            foreach (var identifyResult in results)
            {
                if (identifyResult.Candidates.Length == 0)
                {
                    //Alertar
                    return $"{_context.Persons.Where(x => x.PerName == "DESCONOCIDO").FirstOrDefault().PerId}";
                }
                else
                {
                    // Get top 1 among all candidates returned
                    var candidateId = identifyResult.Candidates[0].PersonId;
                    var person = await faceServiceClient.GetPersonAsync(personGroupId.ToLower(), candidateId);
                    var dni = person.Name.Split('-')[1];

                    return $"{_context.Persons.Where(x => x.IdentificationDocument == dni).FirstOrDefault().PerId}";
                }
            }

            return "";
        }

        //Agregar destino
        public async Task DetectAlarm(string personId, string Icon, string Device, string RecognitionId)
        {
            var findAlarm = _context.Alarms.AsNoTracking().Include(x => x.Sub)
                .Include(x => x.Sub.Com)
                .SingleOrDefault(x => x.PerId == personId);

            if (findAlarm != null)
            {
                var registrationId = Device;

                using (var sender = new Sender("AAAAcPbuCu4:APA91bFIIDkuXGdduhY1YlxmnhcKRa9fXa4ilj8qPx_99Tgeq7k4zaMfMAi_5Lpk9fu8u739FvmyvXcFbJ6Vo3enx6Rl2G5WhY3DFzPSpXlm6o-igpbL4M9XJbPauwbnYZrC9cTFY1pD"))
                {
                    var message = new Message
                    {
                        RegistrationIds = new List<string> { registrationId },
                        Notification = new Notification
                        {
                            Title = $"Alarma {findAlarm.AlmDescription}",
                            Body = $"Se ha disparado la alarma de {findAlarm.AlmDescription} en {findAlarm.Sub.SubDescription} - {findAlarm.Sub.Com.ComDescription}",
                            Icon = Icon,
                            ClickAction = $"https://monitor20180107071656.azurewebsites.net/Recognitions/details/{RecognitionId}"
                        }
                    };
                    var result = await sender.SendAsync(message);
                }

            }

            await Task.Yield();
        }

        public async Task DescribeImage(string imageUrl, string recId)
        {
            var VisionServiceClient = new VisionServiceClient("413d0aea55e84fcaa45b2448651c2184");
            var analysisResult = await VisionServiceClient.DescribeAsync(imageUrl, 3);
            await LogAnalysisResultAsync(analysisResult, recId);
        }

        private async Task LogAnalysisResultAsync(AnalysisResult result, string recId)
        {
            if (result == null)
            {
                return;
            }

            if (result.Description != null)
            {
                foreach (var caption in result.Description.Captions)
                {
                    _context.DescriptionRec.Add(new DescriptionRec
                    {
                        DesId = _generate.GenerateIdAsync("DES", "Detection.DescriptionRec").Result,
                        RecId = recId,
                        RecDescription = caption.Text
                    });
                    await _context.SaveChangesAsync();
                    //r = "  Caption : " + caption.Text + "; Confidence : " + caption.Confidence;
                }
            }
        }
    }
}
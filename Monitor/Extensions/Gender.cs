﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Extensions
{
    public class Gender
    {
        public string Description { get; set; }
        public string Value { get; set; }
    }
}

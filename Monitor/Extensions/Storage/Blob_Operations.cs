﻿using Microsoft.AspNetCore.Http;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Extensions.Storage
{
    public class Blob_Operations
    {
        public async Task DeletePhotoPerson(string image, string account)
        {
            try
            {

                CloudStorageAccount storageAccount = Common.CreateStorageAccountFromConnectionString(account);

                // Create a blob client for interacting with the blob service.

                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                CloudBlobContainer container = blobClient.GetContainerReference("personphotoupload");

                CloudBlockBlob cloudBlockBlob = container.GetBlockBlobReference(image);

                await cloudBlockBlob.DeleteIfExistsAsync();

            }
            catch (Exception ex)
            {
            }

        }

        public async Task<string> UploadPhotoPerson(IFormFile imageToUpload, string account)
        {
            string imageFullPath = null;

            try
            {
                if (imageToUpload == null || imageToUpload.Length == 0)
                {
                    return null;
                }

                CloudStorageAccount storageAccount = Common.CreateStorageAccountFromConnectionString(account);

                // Create a blob client for interacting with the blob service.

                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                CloudBlobContainer container = blobClient.GetContainerReference("personphotoupload");

                if (await container.CreateIfNotExistsAsync())
                {
                    await container.SetPermissionsAsync(
                    new BlobContainerPermissions
                    {
                        PublicAccess = BlobContainerPublicAccessType.Blob
                    }
                    );
                }

                string imageName = Guid.NewGuid().ToString() + "-" + Path.GetExtension(imageToUpload.FileName);

                CloudBlockBlob cloudBlockBlob = container.GetBlockBlobReference(imageName);
                cloudBlockBlob.Properties.ContentType = imageToUpload.ContentType;

                // Create or overwrite the blob with the contents of a local file 
                using (var fileStream = imageToUpload.OpenReadStream())
                {
                    await cloudBlockBlob.UploadFromStreamAsync(fileStream);
                }

                imageFullPath = cloudBlockBlob.Uri.ToString();
            }
            catch (Exception ex)
            {
            }
            return imageFullPath;
        }
    }
}
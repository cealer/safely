﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Extensions
{
    public class Helper_Gender
    {
        public List<Gender> Gender()
        {
            List<Gender> lista = new List<Gender>();
            lista.Add(new Gender { Description = "Masculino", Value = "M" });
            lista.Add(new Gender { Description = "Femenino", Value = "F" });
            return lista;
        }
    }
}
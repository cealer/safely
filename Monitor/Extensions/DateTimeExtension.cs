﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Extensions
{
    public static class DateTimeExtension
    {
        public static DateTime GetUserDateTime(string TimeZone)
        {
            TimeZoneInfo timeInfo = TimeZoneInfo.FindSystemTimeZoneById(TimeZone);
            DateTime userTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeInfo);
            return userTime;
        }
    }
}
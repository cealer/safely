﻿using Microsoft.EntityFrameworkCore;
using Monitor.Data;
using Monitor.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Extensions
{
    public class Generator
    {
        private readonly ApplicationDbContext _context;

        public Generator(ApplicationDbContext context)
        {
            _context = context;

        }

        public string GenerateId(string code, string Table)
        {
            string cod = "";
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = $"SELECT '{code}'+RIGHT('000000000000000' + RTRIM(LTRIM(STR(COUNT(*)+1))),15) FROM {Table}";
                _context.Database.OpenConnection();
                var r = command.ExecuteScalar();
                cod = r.ToString();
            }

            return cod;
        }

        //Generar ID
        public async Task<string> GenerateIdAsync(string code, string Table)
        {
            string cod;
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = $"SELECT '{code}'+RIGHT('000000000000000' + RTRIM(LTRIM(STR(COUNT(*)+1))),15) FROM {Table}";
                _context.Database.OpenConnection();
                var r = await command.ExecuteScalarAsync();
                cod = r.ToString();
            }

            return cod;
        }

    }
}

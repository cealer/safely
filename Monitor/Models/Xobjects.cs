﻿using Monitor.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Monitor.Model
{
    [Table("XObjects", Schema = "Register")]
    public partial class Xobjects
    {
        public Xobjects()
        {
            Alarms = new HashSet<Alarms>();
            DetailsRecognitions = new HashSet<DetailsRecognitions>();
        }

        [Key]
        [Column("Obj_Id", TypeName = "char(18)")]
        public string ObjId { get; set; }
        [Required]
        [Column("Obj_Description")]
        [StringLength(60)]
        [Display(Name = "Descripción")]
        public string ObjDescription { get; set; }
        [Required]
        [Column("Obj_Path")]
        [Display(Name = "Foto")]
        public string ObjPath { get; set; }
        [Column("Obj_Date", TypeName = "datetime")]
        [Display(Name = "Feha de registro")]
        public DateTime ObjDate { get; set; }
        [Required]
        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }
        [Column("Obj_Enabled")]
        public bool ObjEnabled { get; set; }


        [Column("Usr_Id", TypeName = "nvarchar(450)")]
        public string UserId { get; set; }

        [Display(Name = "Registrado por")]
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        [ForeignKey("SubId")]
        [InverseProperty("Xobjects")]
        [Display(Name = "Subárea")]
        public Subsidiaries Sub { get; set; }

        [InverseProperty("Obj")]
        public ICollection<Alarms> Alarms { get; set; }
        [InverseProperty("Obj")]
        public ICollection<DetailsRecognitions> DetailsRecognitions { get; set; }
    }
}
﻿using Monitor.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Monitor.Model
{
    [Table("Companies", Schema = "Register")]
    public partial class Companies
    {
        [Key]
        [Column("Com_Id", TypeName = "char(18)")]
        public string ComId { get; set; }

        [Required]
        [Column("Com_Description")]
        [Display(Name ="Descripción")]
        [StringLength(50)]
        public string ComDescription { get; set; }

        [Display(Name ="Fecha de registro")]
        [Column("Com_Date", TypeName = "datetime")]
        public DateTime ComDate { get; set; }

        [Column("Usr_Id", TypeName = "nvarchar(450)")]
        public string UserId { get; set; }

        [JsonIgnore]
        [Display(Name ="Registrado por")]
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        [Column("Com_Enabled")]
        public bool ComEnabled { get; set; }

        [JsonIgnore]
        [InverseProperty("Com")]
        public ICollection<Subsidiaries> Subsidiaries { get; set; }
    }
}
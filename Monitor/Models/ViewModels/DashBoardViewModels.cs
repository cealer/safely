﻿using Monitor.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models.ViewModels
{
    public class DashBoardViewModels
    {
        public int recs { get; set; }
        public int people { get; set; }
        public int camaras { get; set; }
        public int alarms { get; set; }
        public List<Alarms> LastAlarms { get; set; }
        public List<RecViewModel> listRecs { get; set; }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models.ViewModels
{
    public class DeviceViewModel
    {
        public string DevDescription { get; set; }
        public string CodeId { get; set; }
    }
}
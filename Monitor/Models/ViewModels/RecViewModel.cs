﻿using Monitor.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models.ViewModels
{
    public class RecViewModel
    {
        public string Person { get; set; }
        public string Photo { get; set; }
        public string Com { get; set; }
        public string Sub { get; set; }
        public string Cam { get; set; }

        public DateTime DateRegister { get; set; }
    }
}
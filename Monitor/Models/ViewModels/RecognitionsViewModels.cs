﻿using Monitor.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models.ViewModels
{
    public class RecognitionsViewModels
    {
        public Recognitions Recognition { get; set; }
        public Camaras Camara { get; set; }
        public Subsidiaries Subsidiary { get; set; }
        public Companies Company { get; set; }
        public People Person { get; set; }
        public List<DescriptionRec> Descriptions { get; set; }
    }
}
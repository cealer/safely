﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Monitor.Model
{
    [Table("DetailsAlarms", Schema = "Detection")]
    public partial class DetailsAlarms
    {
        [Key]
        [Column("DAl_Id", TypeName = "char(18)")]
        public string DalId { get; set; }
        [Required]
        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }
        [Required]
        [Column("Alm_Id", TypeName = "char(18)")]
        public string AlmId { get; set; }
        [Column("DAl_Date", TypeName = "datetime")]
        public DateTime DalDate { get; set; }

        [ForeignKey("AlmId")]
        [InverseProperty("DetailsAlarms")]
        public Alarms Alm { get; set; }
        [ForeignKey("SubId")]
        [InverseProperty("DetailsAlarms")]
        public Subsidiaries Sub { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Monitor.Model
{
    [Table("Recognitions", Schema = "Detection")]
    public partial class Recognitions
    {
        public Recognitions()
        {
            DescriptionRec = new HashSet<DescriptionRec>();
            DetailsRecognitions = new HashSet<DetailsRecognitions>();
        }

        [Key]
        [Column("Rec_Id", TypeName = "char(18)")]
        public string RecId { get; set; }
        [Column("Per_Id", TypeName = "char(18)")]
        public string PerId { get; set; }

        [Required]
        [Column("Rec_Path")]
        [Display(Name = "Foto")]
        public string RecPath { get; set; }

        [Display(Name = "Feha de registro")]
        [Column("Rec_Date", TypeName = "datetime")]
        public DateTime RecDate { get; set; }

        [Required]
        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }

        [Required]
        [Column("Cam_Id", TypeName = "char(18)")]
        public string CamId { get; set; }

        [Column("Rec_Enabled")]
        public bool RecEnabled { get; set; }

        [Column("Rec_PathFull")]
        [Display(Name = "Foto completa")]
        public string RecPathFull { get; set; }

        [ForeignKey("CamId")]
        [InverseProperty("Recognitions")]
        [Display(Name = "Cámaras")]
        public Camaras Cam { get; set; }

        [ForeignKey("PerId")]
        [InverseProperty("Recognitions")]
        [Display(Name = "Persona")]
        public People Per { get; set; }

        [Display(Name = "Subárea")]
        [ForeignKey("SubId")]
        [InverseProperty("Recognitions")]
        public Subsidiaries Sub { get; set; }

        [JsonIgnore]
        [InverseProperty("Rec")]
        public ICollection<DescriptionRec> DescriptionRec { get; set; }

        [InverseProperty("Rec")]
        public ICollection<DetailsRecognitions> DetailsRecognitions { get; set; }
    }
}
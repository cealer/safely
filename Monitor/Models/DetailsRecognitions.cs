﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Monitor.Model
{
    [Table("DetailsRecognitions", Schema = "Detection")]
    public partial class DetailsRecognitions
    {
        [Key]
        [Column("Dre_Id", TypeName = "char(18)")]
        public string DreId { get; set; }
        [Required]
        [Column("Rec_Id", TypeName = "char(18)")]
        public string RecId { get; set; }
        [Required]
        [Column("Obj_Id", TypeName = "char(18)")]
        public string ObjId { get; set; }
        [Column("Dre_Percentage", TypeName = "decimal(4, 2)")]
        public decimal DrePercentage { get; set; }
        [Column("Dre_Enabled")]
        public bool DreEnabled { get; set; }
        [Column("Dre_Date", TypeName = "datetime")]
        public DateTime DreDate { get; set; }
        [Column("Alm_Id", TypeName = "char(18)")]
        public string AlmId { get; set; }

        [ForeignKey("AlmId")]
        [InverseProperty("DetailsRecognitions")]
        public Alarms Alm { get; set; }
        [ForeignKey("ObjId")]
        [InverseProperty("DetailsRecognitions")]
        public Xobjects Obj { get; set; }
        [ForeignKey("RecId")]
        [InverseProperty("DetailsRecognitions")]
        public Recognitions Rec { get; set; }
    }
}

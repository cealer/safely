﻿using Monitor.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models
{
    [Table("PhotoPerson", Schema = "Register")]
    public class PhotoPerson
    {
        [Key]
        [Column("Pho_Id", TypeName = "char(18)")]
        public string PhoId { get; set; }

        [Display(Name = "Foto")]
        public string UrlImage { get; set; }

        public string persistedFaceIds { get; set; }

        public string personIdApi { get; set; }

        [Column("Pho_PerId", TypeName = "char(18)")]
        public string PerId { get; set; }

        public DateTime DateRegister { get; set; }

        public bool Enabled { get; set; }

        [Column("Usr_Id", TypeName = "nvarchar(450)")]
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        [ForeignKey("PerId")]
        [InverseProperty("PhotoPersons")]
        public People Per { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Monitor.Model
{
    [Table("NeuralNetworks", Schema = "Register")]
    public partial class NeuralNetworks
    {
        public NeuralNetworks()
        {
            Training = new HashSet<Training>();
        }

        [Key]
        [Column("Neu_Id", TypeName = "char(18)")]
        public string NeuId { get; set; }
        [Required]
        [Column("Neu_Description")]
        [StringLength(50)]
        public string NeuDescription { get; set; }
        [Required]
        [Column("Neu_Path")]
        [StringLength(50)]
        public string NeuPath { get; set; }
        [Column("Neu_Date", TypeName = "datetime")]
        public DateTime NeuDate { get; set; }
        [Required]
        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }
        [Column("Sub_Enabled")]
        public bool SubEnabled { get; set; }

        [ForeignKey("SubId")]
        [InverseProperty("NeuralNetworks")]
        public Subsidiaries Sub { get; set; }
        [InverseProperty("Neu")]
        public ICollection<Training> Training { get; set; }
    }
}

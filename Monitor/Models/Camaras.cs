﻿using Monitor.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Monitor.Model
{
    [Table("Camaras", Schema = "Register")]
    public partial class Camaras
    {
        public Camaras()
        {
            Recognitions = new HashSet<Recognitions>();
        }

        [Key]
        [Column("Cam_Id", TypeName = "char(18)")]
        public string CamId { get; set; }

        [Required]
        [Column("Cam_Description")]
        [StringLength(20)]
        [Display(Name ="Descripción")]
        public string CamDescription { get; set; }

        [Display(Name ="Longitud")]
        [Column("Cam_Longitude")]
        public double? CamLongitude { get; set; }

        [Display(Name ="Fecha de registro")]
        [Column("Cam_Date", TypeName = "datetime")]
        public DateTime CamDate { get; set; }

        [Required]
        [Display(Name ="Subárea")]
        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }

        [Display(Name = "Ruta de conexión")]
        [Required]
        [Column("Cam_Source")]
        [StringLength(200)]
        public string CamSource { get; set; }

        [Column("Cam_Enabled")]
        public bool CamEnabled { get; set; }

        [Display(Name = "Latitud")]
        [Column("Cam_Latitude")]
        public double? CamLatitude { get; set; }

        [Column("Usr_Id", TypeName = "nvarchar(450)")]
        public string UserId { get; set; }

        [JsonIgnore]
        [Display(Name = "Registrado por")]
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        [Display(Name = "Zona Horaria")]
        [Column("CamTimeZone", TypeName = "nvarchar(100)")]
        public string TimeZone { get; set; }

        [JsonIgnore]
        [ForeignKey("CamId")]
        [InverseProperty("InverseCam")]
        [Display(Name = "Cámara")]
        public Camaras Cam { get; set; }

        [JsonIgnore]
        [ForeignKey("SubId")]
        [InverseProperty("Camaras")]
        [Display(Name = "Subárea")]
        public Subsidiaries Sub { get; set; }

        [JsonIgnore]
        [InverseProperty("Cam")]
        public Camaras InverseCam { get; set; }

        [JsonIgnore]
        [InverseProperty("Cam")]
        public ICollection<Recognitions> Recognitions { get; set; }
    }
}
﻿using Monitor.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Monitor.Model
{
    [Table("Persons", Schema = "Register")]
    public partial class People
    {
        public People()
        {
            Alarms = new HashSet<Alarms>();
            Recognitions = new HashSet<Recognitions>();
        }

        [Key]
        [Column("Per_Id", TypeName = "char(18)")]
        public string PerId { get; set; }

        [Required]
        [Column("Per_Name")]
        [StringLength(50)]
        [Display(Name = "Nombres")]
        public string PerName { get; set; }

        [Required]
        [Column("Per_LastName")]
        [StringLength(50)]
        [Display(Name = "Apellidos")]
        public string PerLastName { get; set; }

        [Required]
        [Display(Name = "Documento de identidad")]
        [Column("Per_Identification", TypeName = "nvarchar(50)")]
        public string IdentificationDocument { get; set; }

        [Column("Per_Email")]
        [Display(Name = "Email")]
        [StringLength(50)]
        public string PerEmail { get; set; }

        [Column("Per_Path")]
        [StringLength(50)]
        [Display(Name = "ID")]
        public string PerPath { get; set; }

        [Column("Per_Phone")]
        [StringLength(16)]
        [Display(Name = "Telf.")]
        public string PerPhone { get; set; }

        [Display(Name = "Fecha de registro")]
        [Column("Per_Date", TypeName = "datetime")]
        public DateTime PerDate { get; set; }

        [Required]
        [Display(Name = "Género")]
        [Column("Per_Gender", TypeName = "char(1)")]
        public string PerGender { get; set; }

        [Display(Name = "Fecha de nacimiento")]
        [Column("Per_DateBirth", TypeName = "datetime")]
        public DateTime? PerDateBirth { get; set; }

        [Required]
        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }

        [Column("Cat_Id", TypeName = "char(18)")]
        public string CatId { get; set; }

        [Column("Per_Enabled")]
        public bool PerEnabled { get; set; }

        [Column("Per_Photo")]
        [Display(Name = "Foto")]
        public string PerPhoto { get; set; }

        [Column("Usr_Id", TypeName = "nvarchar(450)")]
        public string UserId { get; set; }

        [JsonIgnore]
        [Display(Name = "Registrado por")]
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        [NotMapped]
        [Display(Name = "Nombres y Apellidos")]
        public string FullName { get { return string.Format("{0} {1}", this.PerName, this.PerLastName); } }

        [JsonIgnore]
        [ForeignKey("CatId")]
        [InverseProperty("Persons")]
        [Display(Name = "Cargo")]
        public Categories Cat { get; set; }

        [JsonIgnore]
        [Display(Name = "Subárea")]
        [ForeignKey("SubId")]
        [InverseProperty("Persons")]
        public Subsidiaries Sub { get; set; }

        [JsonIgnore]
        [InverseProperty("Per")]
        public ICollection<Alarms> Alarms { get; set; }

        [JsonIgnore]
        [InverseProperty("Per")]
        public ICollection<Recognitions> Recognitions { get; set; }

        [JsonIgnore]
        [InverseProperty("Per")]
        public ICollection<PhotoPerson> PhotoPersons { get; set; }

    }
}
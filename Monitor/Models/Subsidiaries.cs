﻿using Monitor.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Monitor.Model
{
    [Table("Subsidiaries", Schema = "Register")]
    public partial class Subsidiaries
    {
        public Subsidiaries()
        {
            Alarms = new HashSet<Alarms>();
            Camaras = new HashSet<Camaras>();
            Categories = new HashSet<Categories>();
            DetailsAlarms = new HashSet<DetailsAlarms>();
            NeuralNetworks = new HashSet<NeuralNetworks>();
            Persons = new HashSet<People>();
            Recognitions = new HashSet<Recognitions>();
            Xobjects = new HashSet<Xobjects>();
        }

        [Key]
        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }

        [Required]
        [Column("Sub_Description")]
        [StringLength(50)]
        [Display(Name = "Descripción")]
        public string SubDescription { get; set; }

        [Display(Name = "Fecha de registro")]
        [Column("Sub_Date", TypeName = "datetime")]
        public DateTime SubDate { get; set; }

        [Required]
        [Column("Com_Id", TypeName = "char(18)")]
        public string ComId { get; set; }

        [Column("Sub_NameLocation")]
        [StringLength(250)]
        [Display(Name = "Dirección")]
        public string SubNameLocation { get; set; }

        [Column("Sub_Enabled")]
        public bool SubEnabled { get; set; }

        [Column("Sub_Latitude")]
        [Display(Name = "Latitud")]
        public double? SubLatitude { get; set; }

        [Column("Sub_Longitude")]
        [Display(Name = "Longitud")]
        public double? SubLongitude { get; set; }

        [Column("Usr_Id", TypeName = "nvarchar(450)")]
        public string UserId { get; set; }

        [JsonIgnore]
        [ForeignKey("UserId")]
        [Display(Name = "Registrado por")]
        public ApplicationUser User { get; set; }

        [ForeignKey("ComId")]
        [Display(Name = "Área")]
        public Companies Com { get; set; }

        [JsonIgnore]
        [InverseProperty("Sub")]
        public ICollection<Alarms> Alarms { get; set; }

        [JsonIgnore]
        [InverseProperty("Sub")]
        public ICollection<Camaras> Camaras { get; set; }

        [JsonIgnore]
        [InverseProperty("Sub")]
        public ICollection<Categories> Categories { get; set; } 

        [InverseProperty("Sub")]
        public ICollection<DetailsAlarms> DetailsAlarms { get; set; }

        [JsonIgnore]
        [InverseProperty("Sub")]
        public ICollection<NeuralNetworks> NeuralNetworks { get; set; }

        [JsonIgnore]
        [InverseProperty("Sub")]
        public ICollection<People> Persons { get; set; }

        [JsonIgnore]
        [InverseProperty("Sub")]
        public ICollection<Recognitions> Recognitions { get; set; }

        [JsonIgnore]
        [InverseProperty("Sub")]
        public ICollection<Xobjects> Xobjects { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Monitor.Model
{
    [Table("DescriptionRec", Schema = "Detection")]
    public partial class DescriptionRec
    {
        [Key]
        [Column("Des_Id", TypeName = "char(18)")]
        public string DesId { get; set; }
        [Required]
        [Column("Rec_Description")]
        [StringLength(250)]
        [Display(Name = "Descripción")]
        public string RecDescription { get; set; }
        [Required]
        [Column("Rec_Id", TypeName = "char(18)")]
        public string RecId { get; set; }

        [JsonIgnore]
        [ForeignKey("RecId")]
        [InverseProperty("DescriptionRec")]
        public Recognitions Rec { get; set; }
    }
}

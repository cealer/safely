﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Monitor.Model
{
    [Table("History_Tables", Schema = "History")]
    public partial class HistoryTables
    {
        [Key]
        [Column("His_Id", TypeName = "char(18)")]
        public string HisId { get; set; }
        [Required]
        [Column("His_Original")]
        public string HisOriginal { get; set; }
        [Required]
        [Column("His_Update")]
        public string HisUpdate { get; set; }
        [Column("His_Date", TypeName = "datetime")]
        public DateTime HisDate { get; set; }
        [Required]
        [Column("His_Table")]
        public string HisTable { get; set; }
    }
}

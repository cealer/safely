﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Monitor.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        [Display(Name = "Nombres")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Apellidos")]
        [Required]
        public string LastName { get; set; }

        [Display(Name = "Zona Horaria")]
        [Required]
        public string TimeZone { get; set; }

        [NotMapped]
        [Display(Name = "Nombres y Apellidos")]
        public string FullName { get { return string.Format("{0} {1}", this.Name, this.LastName); } }

    }
}
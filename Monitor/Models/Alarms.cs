﻿using Monitor.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Monitor.Model
{
    [Table("Alarms", Schema = "Register")]
    public partial class Alarms
    {
        public Alarms()
        {
            DetailsAlarms = new HashSet<DetailsAlarms>();
            DetailsRecognitions = new HashSet<DetailsRecognitions>();
        }

        [Key]
        [Column("Alm_Id", TypeName = "char(18)")]
        public string AlmId { get; set; }

        [Required]
        [Column("Alm_Description")]
        [StringLength(50)]
        [Display(Name = "Descrición")]
        public string AlmDescription { get; set; }

        [Display(Name = "Fecha de registro")]
        [Column("Alm_Date", TypeName = "datetime")]
        public DateTime AlmDate { get; set; }

        [Column("Per_Id", TypeName = "char(18)")]
        public string PerId { get; set; }

        [Column("Obj_Id", TypeName = "char(18)")]
        public string ObjId { get; set; }

        [Required]
        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }

        [Column("Alm_Enabled")]
        public bool AlmEnabled { get; set; }


        [Column("Usr_Id", TypeName = "nvarchar(450)")]
        public string UserId { get; set; }

        [Display(Name = "Registrado por")]
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }


        [ForeignKey("ObjId")]
        [InverseProperty("Alarms")]
        [Display(Name = "Objeto")]
        public Xobjects Obj { get; set; }

        [ForeignKey("PerId")]
        [InverseProperty("Alarms")]
        [Display(Name = "Persona")]
        public People Per { get; set; }

        [ForeignKey("SubId")]
        [InverseProperty("Alarms")]
        [Display(Name = "Subáreas")]
        public Subsidiaries Sub { get; set; }

        [InverseProperty("Alm")]
        public ICollection<DetailsAlarms> DetailsAlarms { get; set; }

        [InverseProperty("Alm")]
        public ICollection<DetailsRecognitions> DetailsRecognitions { get; set; }
    }
}

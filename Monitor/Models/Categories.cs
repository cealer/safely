﻿using Monitor.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Monitor.Model
{
    [Table("Categories", Schema = "Register")]
    public partial class Categories
    {
        public Categories()
        {
            Persons = new HashSet<People>();
        }

        [Key]
        [Column("Cat_Id", TypeName = "char(18)")]
        public string CatId { get; set; }

        [Required]
        [Column("Cat_Description")]
        [Display(Name = "Descripción")]
        [StringLength(50)]
        public string CatDescription { get; set; }

        [Display(Name ="Fecha de registro")]
        [Column("Cat_Date", TypeName = "datetime")]
        public DateTime? CatDate { get; set; }

        [Required]
        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }

        [Column("Cat_Enabled")]
        public bool CatEnabled { get; set; }

        [Column("Usr_Id", TypeName = "nvarchar(450)")]
        public string UserId { get; set; }

        [Display(Name ="Registrado por:")]
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        [Display(Name ="Subárea")]
        [ForeignKey("SubId")]
        [InverseProperty("Categories")]
        public Subsidiaries Sub { get; set; }

        [InverseProperty("Cat")]
        public ICollection<People> Persons { get; set; }
    }
}

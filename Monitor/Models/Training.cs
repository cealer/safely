﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Monitor.Model
{
    [Table("Training", Schema = "Detection")]
    public partial class Training
    {
        [Key]
        [Column("Tra_Id", TypeName = "char(18)")]
        public string TraId { get; set; }
        [Required]
        [Column("Tra_Description")]
        [StringLength(20)]
        public string TraDescription { get; set; }
        [Column("Tra_Date", TypeName = "datetime")]
        public DateTime TraDate { get; set; }
        [Required]
        [Column("Neu_Id", TypeName = "char(18)")]
        public string NeuId { get; set; }
        [Column("Tra_Enabled")]
        public bool TraEnabled { get; set; }
        [Column("Tra_Status")]
        [StringLength(10)]
        public string TraStatus { get; set; }
        [Column("Tra_HighlightedAt", TypeName = "datetime")]
        public DateTime? TraHighlightedAt { get; set; }

        [ForeignKey("NeuId")]
        [InverseProperty("Training")]
        public NeuralNetworks Neu { get; set; }
    }
}

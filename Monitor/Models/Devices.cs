﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models
{
    [Table("Devices", Schema = "Register")]
    public partial class Devices
    {
        [Key]
        [Column("Dev_Id", TypeName = "char(18)")]
        public string DevId { get; set; }

        [Required]
        [Column("Dev_Description")]
        [StringLength(20)]
        [Display(Name = "Descripción")]
        public string DevDescription { get; set; }

        [Display(Name = "Código")]
        public string CodeId { get; set; }

        [Display(Name = "Fecha de registro")]
        [Column("Dev_Date", TypeName = "datetime")]
        public DateTime CamDate { get; set; }

        [Column("Dev_Enabled")]
        public bool DevEnabled { get; set; }

        [Column("Usr_Id", TypeName = "nvarchar(450)")]
        public string UserId { get; set; }

        [Display(Name = "Registrado por")]
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

    }
}
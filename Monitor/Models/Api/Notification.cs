﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models.Api
{
    public class Notification
    {
        public string title { get; set; }
        public string body { get; set; }
        public string click_action { get; set; }
    }
}
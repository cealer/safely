﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models.Api
{
    public class NotificationResponse
    {
        public Notification notification { get; set; }
        public string to { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Monitor.Data;
using Monitor.Models;
using Monitor.Services;
using Microsoft.AspNetCore.ResponseCompression;
using System.IO.Compression;
using Monitor.Extensions;
using Monitor.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;

namespace Monitor
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddResponseCompression(options =>
            {
                options.Providers.Add<BrotliCompressionProvider>();
                options.EnableForHttps = true;
                options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(new[] {
                // Default
                "text/plain",
                "text/css",
                "application/javascript",
                "text/html",
                "application/xml",
                "text/xml",
                "application/json",
                "text/json",
                "application/xhtml+xml",
                "application/font-woff2",
                "application/atom+xml",
                "image/svg+xml",
                "font/woff2",
        });
            });

            services.Configure<GzipCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.Fastest;
            });

            //Compression Gzip
            //        services.AddResponseCompression(options =>
            //        {
            //            options.EnableForHttps = true;
            //            options.Providers.Add<GzipCompressionProvider>();
            //            options.MimeTypes = new[]
            //{
            //    // Default
            //    "text/plain",
            //    "text/css",
            //    "application/javascript",
            //    "text/html",
            //    "application/xml",
            //    "text/xml",
            //    "application/json",
            //    "text/json",

            //    // Custom
            //    "image/svg+xml",
            //    "application/font-woff2",
            //    "font/woff2",
            //    "image/png"
            //};
            //        });

            //services.Configure<GzipCompressionProviderOptions>(options =>
            //{
            //    options.Level = CompressionLevel.Fastest;
            //});

            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new RequireHttpsAttribute());
            });


            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseResponseCompression();

            var options = new RewriteOptions().AddRedirectToHttps();


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseRewriter(options);
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedFor | Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedProto
            });

            app.UseReferrerPolicy(x => x.NoReferrer());

            app.UseCsp(x =>
            {
                x.ScriptSources(t =>
                {
                    t.Self().CustomSources("https://cdn.datatables.net", "https://cdnjs.cloudflare.com"
                        , "https://maxcdn.bootstrapcdn.com", "https://www.gstatic.com", "https://fonts.googleapis.com",
                        "https://cdn.botframework.com");

                });

                x.StyleSources(t =>
                {
                    t.Self().CustomSources("https://fonts.googleapis.com"
                        , "https://cdn.datatables.net", "https://cdnjs.cloudflare.com", "https://maxcdn.bootstrapcdn.com",
                        "https://cdn.botframework.com"
                        );

                });

                x.ImageSources(t =>
                {
                    t.Self().
                    CustomSources("https://monitorblob.blob.core.windows.net/peoplecontainer", 
                    "https://monitorblob.blob.core.windows.net"
                    , "https://cdn.datatables.net/1.10.16/images", "https://cdn.datatables.net");
                });

            });

            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add("X-Frame-Options", "SAMEORIGIN");

                await next();
            });

            app.UseHsts(x => x.MaxAge(days: 365).IncludeSubdomains());

            app.UseXXssProtection(x => x.EnabledWithBlockMode());

            app.UseXContentTypeOptions();


            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = content =>
                {
                    if (!string.IsNullOrEmpty(content.Context.Request.Query["v"]))
                    {
                        content.Context.Response.Headers.Add("cache-control", new[] { "public,max-age=31536000" });
                        content.Context.Response.Headers.Add("Expires", new[] { DateTime.UtcNow.AddYears(1).ToString("R") }); // Format RFC1123
                    }
                }
            });

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
﻿importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-messaging.js');

var config = {
    apiKey: "AIzaSyDYU4gGvgHkjFI6AwqML8N3NH8BYNEFOPo",
    authDomain: "santodomingo-fcf81.firebaseapp.com",
    databaseURL: "https://santodomingo-fcf81.firebaseio.com",
    projectId: "santodomingo-fcf81",
    storageBucket: "santodomingo-fcf81.appspot.com",
    messagingSenderId: "485179132654"
};

firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {

    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = 'Background Message Title';
    const notificationOptions = {
        body: 'Background Message body.',
        icon: '/firebase-logo.png'
    };

    return self.registration.showNotification(notificationTitle,
        notificationOptions);

});
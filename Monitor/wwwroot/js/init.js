﻿'use strict';

const applicationServerPublicKey = 'BGARDFzAbeYjcJIAL-0I5dj9B19W5ge6EGrB9rTFTCYBxvYv52r5j709jLnjKHPvFFsD9AVNAYI8oMR1Fu75SBo';

let isSubscribed = false;
let swRegistration = null;

function urlB64ToUint8Array(base64String) {

    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    const rawData = window.atob(base64);

    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

$(document).ready(function () {

    if (top.location.pathname === '/') {
        if ('serviceWorker' in navigator && 'PushManager' in window) {
            (function () {
                navigator.serviceWorker.register('sw-PWA.NetCore.js?v=10').then(function (registration) {
                    //Registration was successful
                    console.log('ServiceWorker registration successful with scope: ', registration.scope);
                    notify("ServiceWorker registration successful");

                    swRegistration = registration;

                    const messaging = firebase.messaging();

                    messaging.useServiceWorker(swRegistration);

                    messaging.requestPermission()
                        .then(function () {
                            console.log("permiso");
                            return messaging.getToken();
                        })
                        .then(function (token) {

                            document.getElementById("CodeId").value = token;
                            console.log(token);
                        })
                        .catch(function (err) {
                            console.log(err);
                        })

                    messaging.onMessage(function (payload) {
                        console.log("onMessage: ", payload);
                        const notificationTitle = 'Background Message Title';
                        const notificationOptions = {
                            body: 'Background Message body.',
                            icon: '/firebase-logo.png'
                        };
                        alert("ss");
                        //return self.registration.showNotification(notificationTitle,
                        //    notificationOptions);
                    });

                    initialiseUI();
                }, function (err) {
                    console.log('ServiceWorker registration failed: ', err);
                    notify("ServiceWorker registration failed");
                });

                // Subscribe to receive message from service worker

            })();
        } else {
            notify("Service Worker Not Supported!");
            console.log("Service Worker Not Supported!");
        }
    }
});

function initialiseUI() {

    // Set the initial subscription value
    swRegistration.pushManager.getSubscription()
        .then(function (subscription) {
            isSubscribed = !(subscription === null);

            if (isSubscribed) {
                console.log('User IS subscribed.');
            } else {
                console.log('User is NOT subscribed.');
            }

            updateBtn();
        });

    $("#btnPush").click(function () {
        $("#btnPush").prop("disabled", true);

        if (isSubscribed) {
            // TODO: Unsubscribe user
        } else {
            subscribeUser();
        }

    });

    $("#btnSendPush").click(function () {

        if (isSubscribed) {
            var url = '@Url.Action("TriggerPush", "Home")'
            $.post(url, null, function (data, status) { });
        }

    });
}

function updateBtn() {

    if (Notification.permission === 'denied') {
        $("#btnPush").text('Push Messaging Blocked.');
        $("#btnPush").prop("disabled", true);
        $("#btnSendPush").prop("disabled", true);
        updateSubscriptionOnServer(null);
        return;
    }


    if (isSubscribed) {
        $("#btnPush").text("Disable Push Messaging");
        $("#btnSendPush").prop("disabled", false);
    } else {
        $("#btnPush").text("Enable Push Messaging");
        $("#btnSendPush").prop("disabled", true);
    }

    $("#btnPush").prop("disabled", false);
}

function subscribeUser() {

    const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);

    swRegistration.pushManager.subscribe({
        //an admission that you will show a notification every time a push is sent
        userVisibleOnly: true,
        applicationServerKey: applicationServerKey
    })
        .then(function (subscription) {
            console.log('User is subscribed.');

            updateSubscriptionOnServer(subscription);

            isSubscribed = true;

            updateBtn();
        })
        .catch(function (err) {
            console.log('Failed to subscribe the user: ', err);
            updateBtn();
        });
}

function updateSubscriptionOnServer(subscription) {
    // TODO: Send subscription to application server
    if (subscription) {
        var url = '@Url.Action("AddSubscription", "Home")'

        var subscription = JSON.parse(JSON.stringify(subscription));

        console.log(subscription);

        var subObj = { "endpoint": subscription.endpoint, "p256dh": subscription.keys.p256dh, "auth": subscription.keys.auth }

        console.log(subObj);

        try {
            $.post(url, subObj, function (data, status) { });
        } catch (err) {
            console.log(err);
        }

        console.log(JSON.stringify(subscription));
        $("#subscriptionContext").text(JSON.stringify(subscription));
    }
}


//layout

//navigator.serviceWorker.addEventListener('message', function (event) {
//    reply = processMessage(event.data);
//    event.ports[0].postMessage(reply);
//});


// Send message to service worker
function send_message_to_sw(msg) {
    navigator.serviceWorker.controller.postMessage(msg);
}

// Show online/offline status
window.addEventListener('online', updateOnlineStatus);
window.addEventListener('offline', updateOnlineStatus);

function updateOnlineStatus(event) {
    var condition = navigator.onLine ? "Live" : "Currently offline";
    notify(condition);
}

// Notification
function notify(msg) {
    if (!("Notification" in window)) {
        alert("This browser does not support desktop notification");
    } else if (Notification.permission === "granted") {
        // If it's okay let's create a notification
        var notification = new Notification(msg);
    } else if (Notification.permission !== "denied") {
        Notification.requestPermission(function (permission) {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
                var notification = new Notification(msg);
            }
        });
    }
}

function processMessage(msgObj) {
    try {
        if (msgObj.type == 1) {
            notify(msgObj.message);
            return "received";
        }
        console.log(msgObj);
    } catch (err) {
        console.log(err);
    }
}

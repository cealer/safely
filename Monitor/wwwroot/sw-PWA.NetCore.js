var CACHE_NAME = 'v13';
var resTrack = new Map();

var urlsToCache = [
    '/',
    '/Home/Fallback',
    '/dist/vendor/bootstrap/css/bootstrap.css',
    '/css/sb-admin.css',
    '/dist/app.bundle.js',
];

//var ignoreRequests = new RegExp('(' + [
//    '/Home/TriggerPush'].join('(\/?)|\\') + ')$')
var ignoreRequests = new RegExp('(' + [
    '/Device/AddDevice'] + ')$')
// Install

this.addEventListener('install', function (event) {
    event.waitUntil(
        caches.open(CACHE_NAME).then(function (cache) {
            return cache.addAll(urlsToCache);
        })
    );
});

// Fetch
this.addEventListener('fetch', function (event) {

    //if (ignoreRequests.test(event.request.url)) {
    //    console.log('ignored: ', event.request.url)
    //    // request will be networked
    //    return
    //}
    if (ignoreRequests.test(event.request.url)) {
        console.log('ignored: ', event.request.url)
        // request will be networked
        return
    }

    var requestURL = new URL(event.request.url);

    //if (/^\/Companies\//.test(requestURL.pathname)) {

    //Si hay conexi�n traer la �ltima respuesta
    if (navigator.onLine) {

        event.respondWith(
            caches.open(CACHE_NAME).then(function (cache) {
                return fetch(event.request).then(function (response) {
                    cache.put(event.request, response.clone());
                    return response;
                });
            })
        );

    }
    //Si no hay conexi�n usar la �ltima respuesta en cache
    else {
        event.respondWith(
            // Try the cache
            caches.match(event.request).then(function (response) {
                if (response) {
                    return response;
                }
                return fetch(event.request).then(function (response) {
                    if (response.status === 404) {
                        return caches.match('pages/404.html');
                    }
                    return response
                });
            }).catch(function () {
                sendNotification("You are offline, you will be redirected to home page.");
                fallback = self.location.origin + '/Home/Fallback';
                return caches.match(fallback);
            })
        );

        //event.respondWith(
        //caches.open(CACHE_NAME).then(function (cache) {
        //    console.log("Abrio cache");
        //    return cache.match(event.request).then(function (response) {
        //        console.log("encontro cache");
        //        return response || fetch(event.request).then(function (response) {
        //            console.log("devuelve cache");
        //            cache.put(event.request, response.clone());
        //            return response;
        //        });
        //    });
        //})
        //);

    }

    //event.respondWith(
    //    caches.open(CACHE_NAME).then(function (cache) {
    //        return cache.match(event.request).then(function (response) {
    //            var fetchPromise = fetch(event.request).then(function (networkResponse) {
    //                cache.put(event.request, networkResponse.clone());
    //                return networkResponse;
    //            })
    //            return response || fetchPromise;
    //        })
    //    })
    //);


    //}

    //event.respondWith(retrieveFromCache(event));


});


// Catch first strategy
function retrieveFromCache(event) {

    return caches.open(CACHE_NAME).then(function (cache) {

        return cache.match(event.request).then(function (response) {

            if (response) {
                //Actualizar recurso cada vez
                return response;
            }

            console.log('Network request for ', event.request.url);

            if (navigator.onLine) {
                var fetchRequest = event.request.clone();
                return fetch(fetchRequest).then(
                    function (response) {

                        if (!response || response.status !== 200 || response.type !== 'basic') {
                            return response;
                        }

                        var responseToCache = response.clone();
                        cache.put(event.request, responseToCache);
                        resTrack.set(event.request.url, new Date().getTime());
                        console.log(resTrack);
                        return response;
                    });
            } else {
                sendNotification("You are offline, you will be redirected to home page.");
                fallback = self.location.origin + '/Home/Fallback';
                return caches.match(fallback);

            }
        })
    })
}


// Activate
this.addEventListener('activate', function (event) {

    var cacheWhitelist = [CACHE_NAME];

    event.waitUntil(
        caches.keys().then(function (keyList) {
            return Promise.all(keyList.map(function (key) {
                if (cacheWhitelist.indexOf(key) === -1) {
                    return caches.delete(key);
                }
            }));
        })
    );
});

this.addEventListener('message', function (event) {
    processMessage(event.data);
});

// Send to client
function send_message_to_client(client, msg) {
    return new Promise(function (resolve, reject) {
        var msg_chan = new MessageChannel();
        msg_chan.port1.onmessage = function (event) {
            if (event.data.error) {
                reject(event.data.error);
            } else {
                resolve(event.data);
            }
        };
        client.postMessage(msg, [msg_chan.port2]);
    });
}


// Send to all clients
function send_message_to_all_clients(msg) {
    clients.matchAll().then(clients => {
        clients.forEach(client => {
            send_message_to_client(client, msg).then(m => this.processMessage(m));
        })
    })
}


function processMessage(msgObj) {

    try {
        if (msgObj.type == 1) {
            console.log(msgObj.message);
        }
    } catch (err) {
        console.log(err);
    }
}

// Send notification to UI
function sendNotification(msg) {
    var msgObg = { "type": 1, "message": msg }
    send_message_to_all_clients(msgObg);
}


//self.addEventListener('push', function (event) {
//    console.log('[Service Worker] Push Received.');
//    console.log(`[Service Worker] Push had this data: "${event.data.text()}"`);

//    const title = 'Push Codelab';
//    const options = {
//        body: event.data.text()
//    };

//    event.waitUntil(self.registration.showNotification(title, options));
//});
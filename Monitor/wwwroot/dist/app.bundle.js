var ui =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
__webpack_require__(2);
module.exports = __webpack_require__(3);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
﻿

const applicationServerPublicKey = 'BGARDFzAbeYjcJIAL-0I5dj9B19W5ge6EGrB9rTFTCYBxvYv52r5j709jLnjKHPvFFsD9AVNAYI8oMR1Fu75SBo';

let isSubscribed = false;
let swRegistration = null;


function urlB64ToUint8Array(base64String) {

    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    const rawData = window.atob(base64);

    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

$(document).ready(function () {

    if (top.location.pathname === '/') {
        if ('serviceWorker' in navigator && 'PushManager' in window) {
            (function () {
                navigator.serviceWorker.register('/sw-PWA.NetCore.js?v=13').then(function (registration) {
                    //Registration was successful
                    console.log('ServiceWorker registration successful with scope: ', registration.scope);
                    notify("ServiceWorker registration successful");

                    swRegistration = registration;

                    initialiseUI();
                }, function (err) {
                    console.log('ServiceWorker registration failed: ', err);
                    notify("ServiceWorker registration failed");
                });

                // Subscribe to receive message from service worker

            })();
        } else {
            notify("Service Worker Not Supported!");
            console.log("Service Worker Not Supported!");
        }
    }
});

function initialiseUI() {

    // Set the initial subscription value
    swRegistration.pushManager.getSubscription()
        .then(function (subscription) {
            isSubscribed = !(subscription === null);

            if (isSubscribed) {
                console.log('User IS subscribed.');
            } else {
                console.log('User is NOT subscribed.');
            }

            updateBtn();
        });

    $("#btnPush").click(function () {
        $("#btnPush").prop("disabled", true);

        if (isSubscribed) {
            // TODO: Unsubscribe user
        } else {
            subscribeUser();
        }

    });

    $("#btnSendPush").click(function () {

        if (isSubscribed) {
            var url = '@Url.Action("TriggerPush", "Home")'
            $.post(url, null, function (data, status) { });
        }

    });
}

function updateBtn() {

    if (Notification.permission === 'denied') {
        $("#btnPush").text('Push Messaging Blocked.');
        $("#btnPush").prop("disabled", true);
        $("#btnSendPush").prop("disabled", true);
        updateSubscriptionOnServer(null);
        return;
    }


    if (isSubscribed) {
        $("#btnPush").text("Disable Push Messaging");
        $("#btnSendPush").prop("disabled", false);
    } else {
        $("#btnPush").text("Enable Push Messaging");
        $("#btnSendPush").prop("disabled", true);
    }

    $("#btnPush").prop("disabled", false);
}

function subscribeUser() {

    const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);

    swRegistration.pushManager.subscribe({
        //an admission that you will show a notification every time a push is sent
        userVisibleOnly: true,
        applicationServerKey: applicationServerKey
    })
        .then(function (subscription) {
            console.log('User is subscribed.');

            updateSubscriptionOnServer(subscription);

            isSubscribed = true;

            updateBtn();
        })
        .catch(function (err) {
            console.log('Failed to subscribe the user: ', err);
            updateBtn();
        });
}

function updateSubscriptionOnServer(subscription) {
    // TODO: Send subscription to application server
    if (subscription) {
        var url = '@Url.Action("AddSubscription", "Home")'

        var subscription = JSON.parse(JSON.stringify(subscription));

        console.log(subscription);

        var subObj = { "endpoint": subscription.endpoint, "p256dh": subscription.keys.p256dh, "auth": subscription.keys.auth }

        console.log(subObj);

        try {
            $.post(url, subObj, function (data, status) { });
        } catch (err) {
            console.log(err);
        }

        console.log(JSON.stringify(subscription));
        $("#subscriptionContext").text(JSON.stringify(subscription));
    }
}


//layout

//navigator.serviceWorker.addEventListener('message', function (event) {
//    reply = processMessage(event.data);
//    event.ports[0].postMessage(reply);
//});


// Send message to service worker
function send_message_to_sw(msg) {
    navigator.serviceWorker.controller.postMessage(msg);
}

// Show online/offline status
window.addEventListener('online', updateOnlineStatus);
window.addEventListener('offline', updateOnlineStatus);

function updateOnlineStatus(event) {
    var condition = navigator.onLine ? "Live" : "Currently offline";
    notify(condition);
}

// Notification
function notify(msg) {
    if (!("Notification" in window)) {
        alert("This browser does not support desktop notification");
    } else if (Notification.permission === "granted") {
        // If it's okay let's create a notification
        var notification = new Notification(msg);
    } else if (Notification.permission !== "denied") {
        Notification.requestPermission(function (permission) {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
                var notification = new Notification(msg);
            }
        });
    }
}

function processMessage(msgObj) {
    try {
        if (msgObj.type == 1) {
            notify(msgObj.message);
            return "received";
        }
        console.log(msgObj);
    } catch (err) {
        console.log(err);
    }
}


/***/ }),
/* 2 */
/***/ (function(module, exports) {

﻿// Initialize Firebase
var config = {
    apiKey: "AIzaSyDYU4gGvgHkjFI6AwqML8N3NH8BYNEFOPo",
    authDomain: "santodomingo-fcf81.firebaseapp.com",
    databaseURL: "https://santodomingo-fcf81.firebaseio.com",
    projectId: "santodomingo-fcf81",
    storageBucket: "santodomingo-fcf81.appspot.com",
    messagingSenderId: "485179132654"
};

if (top.location.pathname === '/Home/Dashboard') {

    firebase.initializeApp(config);

    const messaging = firebase.messaging();

    messaging.requestPermission()
        .then(function () {
            console.log("permiso");
            return messaging.getToken();
        })
        .then(function (token) {
            if (top.location.pathname === '/Home/Dashboard') {
                document.getElementById("CodeId").value = token;
                console.log(token);
            }
        })
        .catch(function (err) {
            console.log(err);
        })

    messaging.onMessage(function (payload) {
        console.log("onMessage: ", payload);
        const notificationTitle = 'Background Message Title';
        const notificationOptions = {
            body: 'Background Message body.',
            icon: '/firebase-logo.png'
        };
        alert("ss");
        //return self.registration.showNotification(notificationTitle,
        //    notificationOptions);
    });
}

/***/ }),
/* 3 */
/***/ (function(module, exports) {

﻿function initMenu() {
    $('#menu ul').hide();
    $('#menu ul').children('.current').parent().show();
    //$('#menu ul:first').show();
    $('#menu li a').click(
        function () {
            var checkElement = $(this).next();

            if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                return false;
            }
            if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                $('#menu ul:visible').slideUp('normal');
                checkElement.slideDown('normal');
                return false;
            }
        }
    );
}



function Table() {
    var table = $('#example').DataTable({
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal({
                    header: function (row) {
                        var data = row.data();
                        return 'Detalles de ' + data[0];
                    }
                }),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                    tableClass: 'table'
                })
            }
        },
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                text: '<i class="fa fa-files-o"></i>',
                exportOptions: {
                    columns: ':visible'
                },
                titleAttr: 'Copiar'
            }
            ,
            //{
            //    extend: 'csvHtml5',
            //    text: 'CSV',
            //      exportOptions: {
            //        columns: ':visible'
            //    }
            //},
            {
                extend: 'excelHtml5',
                text: '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel',
                exportOptions: {
                    columns: ':visible'
                }
            },

            {
                extend: 'pdfHtml5',
                text: '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'print',
                titleAttr: 'Imprimir',
                text: '<i class="fa fa-print"></i>',
                exportOptions: {
                    columns: ':visible'
                }
            },

            , 'colvis'
        ], language: {
            buttons: {
                copyTitle: 'COPIADO AL PORTAPAPELES',
                copySuccess: {
                    _: '%d registros copiados',
                    1: '1 registro copiado'
                },
                colvis: '<i class="fa fa-columns"></i>'
            },
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },

    });
}


function BOTLoad() {

    const params = BotChat.queryParams(location.search);

    var userName = document.getElementById('userName').value;

    var userId = document.getElementById('userId').value;

    const user = {

        id: userId,

        name: userName

    };

    const bot = {

        id: params['botid'] || 'botid',

        name: params['botname'] || 'botname'

    };

    window.botchatDebug = params['debug'] && params['debug'] === 'true';

    const speechOptions = {

        speechRecognizer: new BotChat.Speech.BrowserSpeechRecognizer(),

        speechSynthesizer: new BotChat.Speech.BrowserSpeechSynthesizer()

    };

    BotChat.App({

        bot: bot,

        //locale: params['locale'],

        resize: 'detect',

        // sendTyping: true,    // defaults to false. set to true to send 'typing' activities to bot (and other users) when user is typing

        speechOptions: speechOptions,

        user: user,

        locale: 'es-es', // override locale to Spanish

        directLine: {

            domain: params['domain'],

            secret: 'CbUQjS98EPc.cwA.uT0.6ZJ60D6DYh51sY_qWSqNETGyhcQJmlhUlCNZUSVCAhs',

            token: params['t'],

            webSocket: params['webSocket'] && params['webSocket'] === 'true' // defaults to true

        }

    }, document.getElementById('chatbot'));

}

$(document).ready(function () {
    if (top.location.pathname !== '/') {
        BOTLoad();
    }

    $('.wc-header').on('click', function () {

        $('.chat').slideToggle(300, 'swing');
        $('.chat-message-counter').fadeToggle(300, 'swing');
    });

    $('#live-chat header').on('click', function () {

        $('.chat').slideToggle(300, 'swing');
        $('.chat-message-counter').fadeToggle(300, 'swing');
    });

    $('.chat-close').on('click', function (e) {

        e.preventDefault();
        $('#live-chat').fadeOut(300);

    });

    initMenu();

    if (top.location.pathname !== '/') {
        Table();
    }

    var url = window.location;
    var element = $('#exampleAccordion a').filter(function () {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).parent().addClass('active');
    if (element.is('li')) {
        element.addClass('active').parent().parent('li').addClass('active')
    }

    //});

    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    $("#menu-toggle-2").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled-2");
        $('#menu ul').hide();
    });

    $('#btnRegister').click(function () {

        let Description = $("#DevDescription").val();
        let CodeId = $("#CodeId").val();
        var token = document.getElementById('CodeId').value;

        let deviceViewModel = {
            DevDescription: Description,
            CodeId: CodeId
        }

        $.ajax({
            type: "POST",
            url: "/Devices/AddDevice",
            dataType: "json",
            data: deviceViewModel,

            headers:
            {
                "RequestVerificationToken": token
            },

            success: function (data) {

                if (data !== null) {
                    window.location = "/Home/Dashboard";
                }
                else {
                    alert("No se pudo registrar");
                }

            },

            failure: function (data) {
                alert('failure ' + data.responseText);
            },
            error: function (data) {
                alert('error' + data.responseText);
            }

        });

    });

    $("#SubId").change(function () {
        $("#PerId").empty();
        let subId = $("#SubId").val();

        $("#PeId").append('<option value="'
            + '">'
            + 'Seleccionar </option>');

        $.ajax({
            type: 'GET',
            url: '/People/getPeople/' + subId,
            dataType: 'json',
            success: function (people) {
                $.each(people, function (i, person) {
                    $("#PerId").append('<option value="'
                        + person.perId + '">'
                        + person.fullName + '</option>');
                });
            },
            error: function (ex) {
                alert('Error.' + ex);
            }
        });

        return false;

    })

    $("#ComId").change(function () {
        $("#SubId").empty();
        $("#CatId").empty();
        let comId = $("#ComId").val();

        $("#SubId").append('<option value="'
            + '">'
            + 'Seleccionar </option>');

        $.ajax({
            type: 'GET',
            url: '/Subsidiaries/GetSubs/' + comId,
            dataType: 'json',
            success: function (subsidiaries) {
                $.each(subsidiaries, function (i, subsidiary) {
                    $("#SubId").append('<option value="'
                        + subsidiary.subId + '">'
                        + subsidiary.subDescription + '</option>');
                });
            },
            error: function (ex) {
                alert('Error.' + ex);
            }
        });

        return false;
    })

    $("#SubId").change(function () {

        let subId = $("#SubId").val()
        $("#CatId").empty();

        $.ajax({
            type: 'GET',
            url: '/Categories/GetCategories/' + subId,
            dataType: 'json',
            success: function (categories) {
                $.each(categories, function (i, category) {
                    if (category == null) {
                        alert("No se encontraron registros.");
                    }
                    $("#CatId").append(
                        '<option value="'
                        + category.catId + '">'
                        + category.catDescription + '</option>'
                    );
                });
            }
            ,
            error: function (ex) {
                alert('Error. ' + ex);
            }
        });

        return false;
    })

});


/***/ })
/******/ ]);
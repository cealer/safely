var ui =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

﻿$(document).ready(function () {

    const params = BotChat.queryParams(location.search);

    var userName = document.getElementById('userName').value;

    var userId = document.getElementById('userId').value;
    
    const user = {

        id: userId,

        name: userName

    };

    const bot = {

        id: params['botid'] || 'botid',

        name: params['botname'] || 'botname'

    };

    window.botchatDebug = params['debug'] && params['debug'] === 'true';

    const speechOptions = {

        speechRecognizer: new BotChat.Speech.BrowserSpeechRecognizer(),

        speechSynthesizer: new BotChat.Speech.BrowserSpeechSynthesizer()

    };

    BotChat.App({

        bot: bot,

        //locale: params['locale'],

        resize: 'detect',

        // sendTyping: true,    // defaults to false. set to true to send 'typing' activities to bot (and other users) when user is typing

        speechOptions: speechOptions,

        user: user,

        locale: 'es-es', // override locale to Spanish

        directLine: {

            domain: params['domain'],

            secret: 'CbUQjS98EPc.cwA.uT0.6ZJ60D6DYh51sY_qWSqNETGyhcQJmlhUlCNZUSVCAhs',

            token: params['t'],

            webSocket: params['webSocket'] && params['webSocket'] === 'true' // defaults to true

        }

    }, document.getElementById('chatbot'));

});

/***/ })
/******/ ]);
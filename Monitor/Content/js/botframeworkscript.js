﻿$(document).ready(function () {

    const params = BotChat.queryParams(location.search);

    var userName = document.getElementById('userName').value;

    var userId = document.getElementById('userId').value;
    
    const user = {

        id: userId,

        name: userName

    };

    const bot = {

        id: params['botid'] || 'botid',

        name: params['botname'] || 'botname'

    };

    window.botchatDebug = params['debug'] && params['debug'] === 'true';

    const speechOptions = {

        speechRecognizer: new BotChat.Speech.BrowserSpeechRecognizer(),

        speechSynthesizer: new BotChat.Speech.BrowserSpeechSynthesizer()

    };

    BotChat.App({

        bot: bot,

        //locale: params['locale'],

        resize: 'detect',

        // sendTyping: true,    // defaults to false. set to true to send 'typing' activities to bot (and other users) when user is typing

        speechOptions: speechOptions,

        user: user,

        locale: 'es-es', // override locale to Spanish

        directLine: {

            domain: params['domain'],

            secret: 'CbUQjS98EPc.cwA.uT0.6ZJ60D6DYh51sY_qWSqNETGyhcQJmlhUlCNZUSVCAhs',

            token: params['t'],

            webSocket: params['webSocket'] && params['webSocket'] === 'true' // defaults to true

        }

    }, document.getElementById('chatbot'));

});
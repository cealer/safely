﻿function initMenu() {
    $('#menu ul').hide();
    $('#menu ul').children('.current').parent().show();
    //$('#menu ul:first').show();
    $('#menu li a').click(
        function () {
            var checkElement = $(this).next();

            if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                return false;
            }
            if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                $('#menu ul:visible').slideUp('normal');
                checkElement.slideDown('normal');
                return false;
            }
        }
    );
}



function Table() {
    var table = $('#example').DataTable({
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal({
                    header: function (row) {
                        var data = row.data();
                        return 'Detalles de ' + data[0];
                    }
                }),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                    tableClass: 'table'
                })
            }
        },
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                text: '<i class="fa fa-files-o"></i>',
                exportOptions: {
                    columns: ':visible'
                },
                titleAttr: 'Copiar'
            }
            ,
            //{
            //    extend: 'csvHtml5',
            //    text: 'CSV',
            //      exportOptions: {
            //        columns: ':visible'
            //    }
            //},
            {
                extend: 'excelHtml5',
                text: '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel',
                exportOptions: {
                    columns: ':visible'
                }
            },

            {
                extend: 'pdfHtml5',
                text: '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'print',
                titleAttr: 'Imprimir',
                text: '<i class="fa fa-print"></i>',
                exportOptions: {
                    columns: ':visible'
                }
            },

            , 'colvis'
        ], language: {
            buttons: {
                copyTitle: 'COPIADO AL PORTAPAPELES',
                copySuccess: {
                    _: '%d registros copiados',
                    1: '1 registro copiado'
                },
                colvis: '<i class="fa fa-columns"></i>'
            },
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },

    });
}


function BOTLoad() {

    const params = BotChat.queryParams(location.search);

    var userName = document.getElementById('userName').value;

    var userId = document.getElementById('userId').value;

    const user = {

        id: userId,

        name: userName

    };

    const bot = {

        id: params['botid'] || 'botid',

        name: params['botname'] || 'botname'

    };

    window.botchatDebug = params['debug'] && params['debug'] === 'true';

    const speechOptions = {

        speechRecognizer: new BotChat.Speech.BrowserSpeechRecognizer(),

        speechSynthesizer: new BotChat.Speech.BrowserSpeechSynthesizer()

    };

    BotChat.App({

        bot: bot,

        //locale: params['locale'],

        resize: 'detect',

        // sendTyping: true,    // defaults to false. set to true to send 'typing' activities to bot (and other users) when user is typing

        speechOptions: speechOptions,

        user: user,

        locale: 'es-es', // override locale to Spanish

        directLine: {

            domain: params['domain'],

            secret: 'CbUQjS98EPc.cwA.uT0.6ZJ60D6DYh51sY_qWSqNETGyhcQJmlhUlCNZUSVCAhs',

            token: params['t'],

            webSocket: params['webSocket'] && params['webSocket'] === 'true' // defaults to true

        }

    }, document.getElementById('chatbot'));

}

$(document).ready(function () {
    if (top.location.pathname !== '/') {
        BOTLoad();
    }

    $('.wc-header').on('click', function () {

        $('.chat').slideToggle(300, 'swing');
        $('.chat-message-counter').fadeToggle(300, 'swing');
    });

    $('#live-chat header').on('click', function () {

        $('.chat').slideToggle(300, 'swing');
        $('.chat-message-counter').fadeToggle(300, 'swing');
    });

    $('.chat-close').on('click', function (e) {

        e.preventDefault();
        $('#live-chat').fadeOut(300);

    });

    initMenu();

    if (top.location.pathname !== '/') {
        Table();
    }

    var url = window.location;
    var element = $('#exampleAccordion a').filter(function () {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).parent().addClass('active');
    if (element.is('li')) {
        element.addClass('active').parent().parent('li').addClass('active')
    }

    //});

    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    $("#menu-toggle-2").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled-2");
        $('#menu ul').hide();
    });

    $('#btnRegister').click(function () {

        let Description = $("#DevDescription").val();
        let CodeId = $("#CodeId").val();
        var token = document.getElementById('CodeId').value;

        let deviceViewModel = {
            DevDescription: Description,
            CodeId: CodeId
        }

        $.ajax({
            type: "POST",
            url: "/Devices/AddDevice",
            dataType: "json",
            data: deviceViewModel,

            headers:
            {
                "RequestVerificationToken": token
            },

            success: function (data) {

                if (data !== null) {
                    window.location = "/Home/Dashboard";
                }
                else {
                    alert("No se pudo registrar");
                }

            },

            failure: function (data) {
                alert('failure ' + data.responseText);
            },
            error: function (data) {
                alert('error' + data.responseText);
            }

        });

    });

    $("#SubId").change(function () {
        $("#PerId").empty();
        let subId = $("#SubId").val();

        $("#PeId").append('<option value="'
            + '">'
            + 'Seleccionar </option>');

        $.ajax({
            type: 'GET',
            url: '/People/getPeople/' + subId,
            dataType: 'json',
            success: function (people) {
                $.each(people, function (i, person) {
                    $("#PerId").append('<option value="'
                        + person.perId + '">'
                        + person.fullName + '</option>');
                });
            },
            error: function (ex) {
                alert('Error.' + ex);
            }
        });

        return false;

    })

    $("#ComId").change(function () {
        $("#SubId").empty();
        $("#CatId").empty();
        let comId = $("#ComId").val();

        $("#SubId").append('<option value="'
            + '">'
            + 'Seleccionar </option>');

        $.ajax({
            type: 'GET',
            url: '/Subsidiaries/GetSubs/' + comId,
            dataType: 'json',
            success: function (subsidiaries) {
                $.each(subsidiaries, function (i, subsidiary) {
                    $("#SubId").append('<option value="'
                        + subsidiary.subId + '">'
                        + subsidiary.subDescription + '</option>');
                });
            },
            error: function (ex) {
                alert('Error.' + ex);
            }
        });

        return false;
    })

    $("#SubId").change(function () {

        let subId = $("#SubId").val()
        $("#CatId").empty();

        $.ajax({
            type: 'GET',
            url: '/Categories/GetCategories/' + subId,
            dataType: 'json',
            success: function (categories) {
                $.each(categories, function (i, category) {
                    if (category == null) {
                        alert("No se encontraron registros.");
                    }
                    $("#CatId").append(
                        '<option value="'
                        + category.catId + '">'
                        + category.catDescription + '</option>'
                    );
                });
            }
            ,
            error: function (ex) {
                alert('Error. ' + ex);
            }
        });

        return false;
    })

});

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Monitor.Model;
using Microsoft.AspNetCore.Authorization;
using Monitor.Extensions;
using Microsoft.AspNetCore.Identity;
using Monitor.Models;
using Monitor.Data;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.ProjectOxford.Face;

namespace Monitor.Controllers
{
    [Authorize]
    public class CompaniesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly Generator _generator;
        private readonly UserManager<ApplicationUser> _userManager;

        public CompaniesController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _generator = new Generator(_context);
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            return View(await _context.Companies.AsNoTracking().Include(x=>x.User).
                Where(x => x.ComEnabled == true && x.UserId == user.Id).ToListAsync());
        }

        public async Task<IActionResult> Details(string id)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (id == null)
            {
                return NotFound();
            }

            var companies = await _context.Companies
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.ComId == id
                && m.ComEnabled == true
                && m.UserId == user.Id);

            if (companies == null)
            {
                return NotFound();
            }

            if (companies.UserId != user.Id)
            {
                return NotFound();
            }

            return View(companies);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ComDescription")] Companies companies)
        {
            if (ModelState.IsValid)
            {

                var user = await _userManager.GetUserAsync(HttpContext.User);
                companies.ComDate = DateTimeExtension.GetUserDateTime(user.TimeZone);
                companies.ComEnabled = true;
                companies.UserId = user.Id;
                companies.ComId = await _generator.GenerateIdAsync("COM", "Register.Companies");
                _context.Add(companies);
                await _context.SaveChangesAsync();

                const string subscriptionKey = "3cdd72f7d08b4deeb10532e59fb2193f";
                var faceServiceClient = new FaceServiceClient(subscriptionKey);
                await faceServiceClient.CreatePersonGroupAsync(companies.ComId.ToLower(), $"{companies.ComDescription}");

                return RedirectToAction(nameof(Index));
            }
            return View(companies);
        }

        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(HttpContext.User);

            var companies = await _context.Companies.AsNoTracking().
                SingleOrDefaultAsync(m => m.ComId == id && m.ComEnabled == true && m.UserId == user.Id);

            if (companies == null)
            {
                return NotFound();
            }

            if (companies.UserId != user.Id)
            {
                return NotFound();
            }
            return View(companies);
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPost(string id)
        {

            var user = await _userManager.GetUserAsync(HttpContext.User);

            var updateModel = await _context.Companies
                .Include(x => x.User)
                .SingleOrDefaultAsync(c => c.ComId == id);

            if (updateModel.UserId != user.Id)
            {
                return NotFound();
            }
            if (id != updateModel.ComId)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                if (await TryUpdateModelAsync<Companies>(updateModel, "", c => c.ComDescription))
                {
                    try
                    {
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateException)
                    {
                        if (!CompaniesExists(updateModel.ComId))
                        {
                            return NotFound();
                        }

                        ModelState.AddModelError("", "No se pudo guardar los cambios. " +
           "Intente de nuevo, y si el problema persiste, " +
           "Comuníquese con el administrador del sistema.");
                    }
                    return RedirectToAction(nameof(Index));
                }

                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);

                foreach (var item in allErrors)
                {
                    ModelState.AddModelError("", item.ErrorMessage);
                }

            }
            return View(updateModel);
        }

        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(HttpContext.User);

            var companies = await _context.Companies.AsNoTracking()
                .SingleOrDefaultAsync(m => m.ComId == id && m.ComEnabled == true);

            if (companies == null)
            {
                return NotFound();
            }

            if (companies.UserId != user.Id)
            {
                return NotFound();
            }

            return View(companies);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var companies = await _context.Companies.SingleOrDefaultAsync(m => m.ComId == id && m.ComEnabled == true);

            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (companies.UserId != user.Id)
            {
                return NotFound();
            }

            companies.ComEnabled = false;

            if (await TryUpdateModelAsync<Companies>(companies, "", c => c.ComEnabled))
            {
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {
                    if (!CompaniesExists(companies.ComId))
                    {
                        return NotFound();
                    }

                    ModelState.AddModelError("", "No se pudo eliminar el registro. " +
       "Intente de nuevo, y si el problema persiste, " +
       "Comuníquese con el administrador del sistema.");
                }
                return RedirectToAction(nameof(Index));
            }

            return NotFound();
        }

        private bool CompaniesExists(string id)
        {
            return _context.Companies.AsNoTracking().Any(e => e.ComId == id && e.ComEnabled == true);
        }
    }
}
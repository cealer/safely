﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Monitor.Data;
using Monitor.Model;

namespace Monitor.Controllers.api
{
    [Produces("application/json")]
    [Route("api/Camaras")]
    public class CamarasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CamarasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Camaras
        //[HttpGet]
        //public IEnumerable<Camaras> GetCamaras()
        //{
        //    return _context.Camaras;
        //}

        // GET: api/Camaras/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCamaras([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var camaras = await _context.Camaras.SingleOrDefaultAsync(m => m.CamId == id);

            if (camaras == null)
            {
                return NotFound();
            }

            return Json(camaras.CamSource);
        }
        
        private bool CamarasExists(string id)
        {
            return _context.Camaras.Any(e => e.CamId == id);
        }
    }
}
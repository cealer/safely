﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Monitor.Model;
using Microsoft.AspNetCore.Authorization;
using Monitor.Data;
using Monitor.Extensions;
using Microsoft.AspNetCore.Identity;
using Monitor.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Monitor.Controllers
{
    [Authorize]
    public class CamarasController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly Generator _generator;
        private readonly UserManager<ApplicationUser> _userManager;

        public CamarasController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _generator = new Generator(_context);
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var monitorContext = _context.Camaras
                .Include(c => c.Cam)
                .Include(c => c.Sub)
                .Include(c => c.User)
                .AsNoTracking()
                .Where(x => x.UserId == user.Id && x.CamEnabled == true);
            return View(await monitorContext.ToListAsync());
        }

        public async Task<IActionResult> Details(string id)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (id == null)
            {
                return NotFound();
            }

            var camaras = await _context.Camaras
                .Include(c => c.Cam)
                .Include(c => c.Sub)
                .Include(c => c.User)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.CamId == id && m.CamEnabled == true);

            if (camaras.UserId != user.Id)
            {
                return NotFound();
            }

            if (camaras == null)
            {
                return NotFound();
            }

            return View(camaras);
        }

        public IActionResult Create()
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            var companies = _context.Companies.Where(x => x.ComEnabled == true && x.UserId == user.Id);

            var company = companies.FirstOrDefault();

            if (company == null)
            {
                return RedirectToAction("Index", "Companies");
            }

            var subsidiaries = _context.Subsidiaries.Where(x => x.ComId == company.ComId && x.UserId == user.Id);

            var zones = TimeZoneInfo.GetSystemTimeZones();

            ViewData["TimeZone"] = new SelectList(zones, "Id", "DisplayName",user.TimeZone);

            ViewData["ComId"] = new SelectList(companies, "ComId", "ComDescription");
            ViewData["SubId"] = new SelectList(subsidiaries, "SubId", "SubDescription");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CamDescription,CamLongitude,SubId,CamSource,CamLatitude,TimeZone")] Camaras camaras)
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            if (ModelState.IsValid)
            {
                camaras.CamId = await _generator.GenerateIdAsync("CAM", "Register.Camaras");
                camaras.CamDate = DateTimeExtension.GetUserDateTime(user.TimeZone);
                camaras.CamEnabled = true;
                camaras.UserId = user.Id;
                _context.Add(camaras);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            FillComSub(camaras, user.Id);

            var zones = TimeZoneInfo.GetSystemTimeZones();
            ViewData["TimeZone"] = new SelectList(zones, "Id", "DisplayName", camaras.TimeZone);

            return View(camaras);
        }

        private void FillComSub(Camaras camaras, string userId)
        {
            var sub = _context.Subsidiaries.Where(x => x.SubId == camaras.SubId).Include(x => x.Com).FirstOrDefault();
            ViewData["ComId"] = new SelectList(_context.Companies.Where(x => x.ComEnabled == true
            && x.UserId == userId), "ComId", "ComDescription", sub.ComId);
            ViewData["SubId"] = new SelectList(_context.Subsidiaries.Where(x => x.SubEnabled == true 
            && x.UserId == userId
            && x.ComId==sub.ComId), "SubId", "SubDescription", camaras.SubId);
        }

        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(HttpContext.User);

            var camaras = await _context.Camaras
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.CamId == id
                && m.CamEnabled == true
                && m.UserId == user.Id);

            if (camaras == null)
            {
                return NotFound();
            }

            if (camaras.UserId != user.Id)
            {
                return NotFound();
            }

            var zones = TimeZoneInfo.GetSystemTimeZones();

            ViewData["TimeZone"] = new SelectList(zones, "Id", "DisplayName");

            FillComSub(camaras, user.Id);

            return View(camaras);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, Camaras camaras)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            var updateModel = await _context.Camaras
                .Include(x => x.User)
                .SingleOrDefaultAsync(c => c.CamId == id
                && c.CamEnabled == true && c.UserId == user.Id);

            if (updateModel.UserId != user.Id)
            {
                return NotFound();
            }

            if (id != updateModel.CamId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (await TryUpdateModelAsync<Camaras>(updateModel, "", c => c.CamDescription,
                    c => c.SubId, c => c.CamLatitude, c => c.CamLongitude, c => c.CamSource))
                {
                    try
                    {
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateException)
                    {

                        if (!CamarasExists(updateModel.CamId))
                        {
                            return NotFound();
                        }

                        ModelState.AddModelError("", "No se pudo guardar los cambios. " +
           "Intente de nuevo, y si el problema persiste, " +
           "Comuníquese con el administrador del sistema.");

                    }

                    return RedirectToAction(nameof(Index));

                }

                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);

                foreach (var item in allErrors)
                {
                    ModelState.AddModelError("", item.ErrorMessage);
                }

            }

            FillComSub(camaras, user.Id);

            var zones = TimeZoneInfo.GetSystemTimeZones();

            ViewData["TimeZone"] = new SelectList(zones, "Id", "DisplayName", updateModel.TimeZone);

            return View(camaras);
        }

        // GET: Camaras/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(HttpContext.User);

            var camaras = await _context.Camaras
                .Include(c => c.Cam)
                .Include(c => c.Sub).
                AsNoTracking()
                .SingleOrDefaultAsync(m => m.CamId == id
                && m.UserId == user.Id
                && m.CamEnabled == true);

            if (camaras == null)
            {
                return NotFound();
            }

            return View(camaras);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            var camaras = await _context.Camaras.SingleOrDefaultAsync(m => m.CamId == id
            && m.UserId == user.Id
            && m.CamEnabled == true);

            _context.Camaras.Remove(camaras);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CamarasExists(string id)
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            return _context.Camaras.AsNoTracking().Any(e => e.CamId == id && e.CamEnabled == true && e.UserId == user.Id);
        }
    }
}
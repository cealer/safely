﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Monitor.Model;
using Monitor.Data;
using Monitor.Extensions;
using Microsoft.AspNetCore.Identity;
using Monitor.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Authorization;

namespace Monitor.Controllers
{
    [Authorize]
    public class AlarmsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly Generator _generator;
        private readonly UserManager<ApplicationUser> _userManager;


        public AlarmsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _generator = new Generator(_context);
            _userManager = userManager;
        }

        // GET: Alarms
        public async Task<IActionResult> Index()
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            var monitorContext = _context.Alarms.Include(a => a.Obj)
                .Include(a => a.Per)
                .Include(a => a.Sub)
                .Include(x => x.User)
                .AsNoTracking().Where(x => x.AlmEnabled == true && x.UserId == user.Id);
            return View(await monitorContext.ToListAsync());
        }

        // GET: Alarms/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            var alarms = await _context.Alarms
                .Include(a => a.Obj)
                .Include(a => a.Per)
                .Include(a => a.Sub)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.AlmId == id);

            if (alarms == null)
            {
                return NotFound();
            }

            return View(alarms);
        }

        public IActionResult Create()
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;
            var companies = _context.Companies.AsNoTracking().Where(x => x.ComEnabled == true && x.UserId == user.Id);

            var company = companies.FirstOrDefault();

            if (company == null)
            {
                return RedirectToAction("Index", "Companies");
            }

            var subsidiaries = _context.Subsidiaries.Where(x => x.ComId == company.ComId && x.UserId == user.Id);
            ViewData["ComId"] = new SelectList(companies, "ComId", "ComDescription");
            ViewData["SubId"] = new SelectList(subsidiaries, "SubId", "SubDescription");

            //ViewData["ObjId"] = new SelectList(_context.Xobjects, "ObjId", "ObjId");
            ViewData["PerId"] = new SelectList(_context.Persons.Where(x => x.PerEnabled == true &&
            x.UserId == user.Id
            && x.SubId == subsidiaries.FirstOrDefault().SubId)
            , "PerId", "FullName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AlmDescription,PerId,SubId")] Alarms alarms)
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            if (ModelState.IsValid)
            {
                alarms.AlmId = await _generator.GenerateIdAsync("ALM", "Register.Alarms");
                alarms.AlmEnabled = true;
                alarms.UserId = user.Id;
                alarms.AlmDate = DateTimeExtension.GetUserDateTime(user.TimeZone);
                _context.Add(alarms);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewData["PerId"] = new SelectList(_context.Persons.Where(x => x.PerEnabled == true &&
x.UserId == user.Id
&& x.SubId == alarms.SubId), "PerId", "FullName", alarms.PerId);

            FillComSub(alarms);

            return View(alarms);

        }

        private void FillComSub(Alarms alarms)
        {
            var sub = _context.Subsidiaries.AsNoTracking().Where(x => x.SubId == alarms.SubId).Include(x => x.Com).FirstOrDefault();
            ViewData["ComId"] = new SelectList(_context.Companies, "ComId", "ComDescription", sub.ComId);
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubDescription", alarms.SubId);
        }

        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            var alarms = await _context.Alarms.AsNoTracking().SingleOrDefaultAsync(m => m.AlmId == id
            && m.AlmEnabled == true
            && m.UserId == user.Id);

            if (alarms == null)
            {
                return NotFound();
            }

            ViewData["PerId"] = new SelectList(_context.Persons.AsNoTracking().Where(x => x.UserId == user.Id
            && x.PerEnabled == true), "PerId", "FullName", alarms.PerId);

            FillComSub(alarms);

            return View(alarms);
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPost(string id)
        {

            var user = await _userManager.GetUserAsync(HttpContext.User);

            var updateModel = await _context.Alarms
                .Include(x => x.User)
                .SingleOrDefaultAsync(c => c.AlmId == id
                && c.AlmEnabled == true && c.UserId == user.Id);

            if (updateModel.UserId != user.Id)
            {
                return NotFound();
            }

            if (id != updateModel.AlmId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (await TryUpdateModelAsync<Alarms>(updateModel, "", c => c.AlmDescription, c => c.SubId, c => c.PerId))
                {
                    try
                    {
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateException)
                    {

                        if (!AlarmsExists(updateModel.AlmId))
                        {
                            return NotFound();
                        }

                        ModelState.AddModelError("", "No se pudo guardar los cambios. " +
           "Intente de nuevo, y si el problema persiste, " +
           "Comuníquese con el administrador del sistema.");

                    }

                    return RedirectToAction(nameof(Index));

                }

                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);

                foreach (var item in allErrors)
                {
                    ModelState.AddModelError("", item.ErrorMessage);
                }

            }

            ViewData["PerId"] = new SelectList(_context.Persons.AsNoTracking().Where(x => x.UserId == user.Id
                && x.PerEnabled == true), "PerId", "FullName", updateModel.PerId);

            FillComSub(updateModel);

            return View(updateModel);
        }

        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(HttpContext.User);

            var alarms = await _context.Alarms
                .Include(a => a.Obj)
                .Include(a => a.Per)
                .Include(a => a.Sub)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.AlmId == id
                && m.AlmEnabled == true
                && m.UserId == user.Id);

            if (alarms == null)
            {
                return NotFound();
            }

            return View(alarms);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var alarms = await _context.Alarms.SingleOrDefaultAsync(m => m.AlmId == id && m.AlmEnabled == true);

            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (alarms.UserId != user.Id)
            {
                return NotFound();
            }

            alarms.AlmEnabled = false;

            if (await TryUpdateModelAsync<Alarms>(alarms, "", c => c.AlmEnabled))
            {
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {
                    if (!AlarmsExists(alarms.AlmId))
                    {
                        return NotFound();
                    }

                    ModelState.AddModelError("", "No se pudo eliminar el registro. " +
       "Intente de nuevo, y si el problema persiste, " +
       "Comuníquese con el administrador del sistema.");
                }
                return RedirectToAction(nameof(Index));
            }

            return NotFound();
        }

        private bool AlarmsExists(string id)
        {
            return _context.Alarms.Any(e => e.AlmId == id);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Monitor.Model;
using Monitor.Extensions;
using Microsoft.AspNetCore.Identity;
using Monitor.Models;
using Monitor.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Monitor.Controllers
{
    [Authorize]
    [Route("Subsidiaries")]
    public class SubsidiariesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly Generator _generator;
        private readonly UserManager<ApplicationUser> _userManager;

        public SubsidiariesController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _generator = new Generator(_context);
            _userManager = userManager;
        }

        [HttpGet("Index/{ComId}")]
        public async Task<IActionResult> Index(string ComId)
        {
            if (ComId == null)
            {
                NotFound();
            }

            //Get user information
            var user = await _userManager.GetUserAsync(HttpContext.User);
            //Get userId from Company 
            var compUsr = await _context.Companies.Include(x=>x.User).AsNoTracking().SingleOrDefaultAsync(x => x.ComId == ComId && x.ComEnabled == true);
            //Validate user's permission to see their subsidiaries(Branch offices)
            if (compUsr.UserId == user.Id)
            {
                ViewData["ComId"] = ComId;

                var subs = await _context.Subsidiaries
                    .AsNoTracking().Include(x=>x.User).Include(x=>x.Com)
                    .Where(x => x.ComId == ComId && x.SubEnabled == true && x.UserId==user.Id)
                    .ToListAsync();

                return View(subs);
            }

            return NotFound();
        }

        [HttpGet("Details/{ComId}/{id}")]
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //Get user information
            var user = await _userManager.GetUserAsync(HttpContext.User);

            var subsidiaries = await _context.Subsidiaries.AsNoTracking()
            .SingleOrDefaultAsync(m => m.SubId == id && m.SubEnabled == true);

            //Validate user's permission to see their subsidiary(Branch office)
            if (subsidiaries.UserId == user.Id)
            {
                if (subsidiaries == null)
                {
                    return NotFound();
                }

                return View(subsidiaries);
            }

            return NotFound();
        }

        [Route("Create/{ComId}")]
        [HttpGet("Create/{ComId}")]
        public async Task<IActionResult> Create(string ComId)
        {
            if (ComId == null)
            {
                NotFound();
            }

            //Get user information
            var user = await _userManager.GetUserAsync(HttpContext.User);
            //Get userId from Company 
            var compUsr = await _context.Companies.AsNoTracking().SingleOrDefaultAsync(x => x.ComId == ComId && x.ComEnabled == true);

            //Validate user's permission to see if he can create a subsidiary(Branch office)
            if (compUsr.UserId == user.Id)
            {
                ViewData["ComId"] = ComId;
                return View();
            }
            return NotFound();
        }

        [Route("Create/{ComId}")]
        [HttpPost("Create/{ComId}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SubDescription,SubNameLocation,SubLatitude,SubLongitude,ComId")] Subsidiaries subsidiaries)
        {
            //Get user information
            var user = await _userManager.GetUserAsync(HttpContext.User);
            //Get userId from Company 
            var compUsr = await _context.Companies.AsNoTracking().SingleOrDefaultAsync(x => x.ComId == subsidiaries.ComId && x.ComEnabled == true);

            //Validate user's permission to see if he can create a subsidiary(Branch office)
            if (compUsr.UserId == user.Id)
            {
                if (ModelState.IsValid)
                {
                    subsidiaries.SubId = await _generator.GenerateIdAsync("SUB", "Register.Subsidiaries");
                    subsidiaries.ComId = subsidiaries.ComId;
                    subsidiaries.SubDate = DateTimeExtension.GetUserDateTime(user.TimeZone);
                    subsidiaries.SubEnabled = true;
                    subsidiaries.UserId = user.Id;
                    _context.Add(subsidiaries);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                return View(subsidiaries);
            }
            else
            {
                NotFound();
            }

            return View();
        }

        [Route("Edit/{ComId}/{id}")]
        [HttpGet("Edit/{ComId}/{id}")]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subsidiaries = await _context.Subsidiaries.AsNoTracking().SingleOrDefaultAsync(m => m.SubId == id && m.SubEnabled == true);

            //Get user information
            var user = await _userManager.GetUserAsync(HttpContext.User);
            //Get userId from Company 
            var compUsr = await _context.Companies.AsNoTracking().SingleOrDefaultAsync(x => x.ComEnabled == true && x.ComId == subsidiaries.ComId);

            if (compUsr.ComEnabled == false)
            {
                return NotFound();
            }

            //Validate user's permission to see if he can create a subsidiary(Branch office)
            if (compUsr.UserId == user.Id)
            {
                if (subsidiaries == null)
                {
                    return NotFound();
                }
                ViewData["ComId"] = new SelectList(_context.Companies.Where(x => x.UserId == compUsr.UserId
            && x.ComEnabled == true), "ComId", "ComDescription", compUsr.ComId);
                return View(subsidiaries);
            }

            return NotFound();
        }

        [Route("Edit/{ComId}/{id}")]
        [HttpPost("Edit/{ComId}/{id}"), ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPost(string id)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            var updateModel = await _context.Subsidiaries
                .Include(x => x.User).
                Include(x => x.Com)
                .SingleOrDefaultAsync(c => c.SubId == id && c.SubEnabled == true);

            if (updateModel.UserId != user.Id)
            {
                return NotFound();
            }

            if (id != updateModel.SubId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (await TryUpdateModelAsync<Subsidiaries>(updateModel, "",
                                    c => c.SubDescription, c => c.SubLatitude, c => c.SubLongitude, c => c.SubNameLocation, c => c.ComId))
                {
                    try
                    {
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateException)
                    {
                        if (!SubsidiariesExists(updateModel.SubId))
                        {
                            return NotFound();
                        }

                        ModelState.AddModelError("", "No se pudo guardar los cambios. " +
           "Intente de nuevo, y si el problema persiste, " +
           "Comuníquese con el administrador del sistema.");
                    }
                    return RedirectToAction(nameof(Index));
                }
            }

            IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);

            foreach (var item in allErrors)
            {
                ModelState.AddModelError("", item.ErrorMessage);
            }
            ViewData["ComId"] = new SelectList(_context.Companies.Where(x => x.UserId == updateModel.UserId
&& x.ComEnabled == true), "ComId", "ComDescription", updateModel.ComId);
            return View(updateModel);

        }

        [HttpGet("Delete/{ComId}/{id}")]
        public async Task<IActionResult> Delete(string id)
        {

            //Get user information
            var user = await _userManager.GetUserAsync(HttpContext.User);

            var subsidiaries = await _context.Subsidiaries.AsNoTracking()
            .SingleOrDefaultAsync(m => m.SubId == id && m.SubEnabled == true);

            //Validate user's permission to see their subsidiary(Branch office)
            if (subsidiaries.UserId == user.Id)
            {

                if (id == null)
                {
                    return NotFound();
                }

                if (subsidiaries == null)
                {
                    return NotFound();
                }
                return View(subsidiaries);
            }

            return NotFound();
        }

        [HttpPost("Delete/{ComId}/{id}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            var updateModel = await _context.Subsidiaries.AsNoTracking().SingleOrDefaultAsync(c => c.SubId == id && c.SubEnabled == true);

            if (updateModel.UserId != user.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (await TryUpdateModelAsync<Subsidiaries>(updateModel, "",
                                    c => c.SubEnabled))
                {
                    try
                    {
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateException)
                    {
                        if (!SubsidiariesExists(updateModel.SubId))
                        {
                            return NotFound();
                        }

                        ModelState.AddModelError("", "No se pudo guardar los cambios. " +
           "Intente de nuevo, y si el problema persiste, " +
           "Comuníquese con el administrador del sistema.");
                    }
                    return RedirectToAction(nameof(Index));
                }
            }

            return NotFound();
        }

        private bool SubsidiariesExists(string id)
        {
            return _context.Subsidiaries.AsNoTracking().Any(e => e.SubId == id && e.SubEnabled == true);
        }

        [Route("GetSubs/{comId}")]
        [HttpGet("GetSubs/{comId}")]
        public async Task<JsonResult> GetSubs(string comId)
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;
            var list = await _context.Subsidiaries
                  .AsNoTracking()
                  .Where(x => x.SubEnabled == true
                  && x.ComId == comId
                  && x.UserId == user.Id).ToListAsync();

            return Json(list);
        }

    }
}
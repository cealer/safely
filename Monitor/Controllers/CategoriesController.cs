﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Monitor.Model;
using Monitor.Data;
using Monitor.Extensions;
using Microsoft.AspNetCore.Identity;
using Monitor.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Monitor.Controllers
{
    [Route("Categories")]
    public class CategoriesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly Generator _generator;
        private readonly UserManager<ApplicationUser> _userManager;

        public CategoriesController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _generator = new Generator(_context);
            _userManager = userManager;
        }

        [HttpGet("Index")]
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            var monitorContext = _context.Categories
                .AsNoTracking()
                .Include(c => c.Sub)
                .Include(c => c.User)
                .Where(x => x.CatEnabled == true && x.UserId == user.Id);

            return View(await monitorContext.ToListAsync());
        }

        [HttpGet("Details/{id}")]
        [Route("Details/{id}")]
        public async Task<IActionResult> Details(string id)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (id == null)
            {
                return NotFound();
            }

            var categories = await _context.Categories
                .Include(c => c.Sub)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.CatId == id && m.CatEnabled == true && m.UserId == user.Id);

            if (categories.UserId != user.Id)
            {
                return NotFound();
            }

            if (categories == null)
            {
                return NotFound();
            }

            return View(categories);
        }

        [HttpGet("Create")]
        [Route("Create")]
        public IActionResult Create()
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;
            var companies = _context.Companies.Where(x => x.ComEnabled == true && x.UserId == user.Id);

            var company = companies.FirstOrDefault();

            if (company == null)
            {
                return RedirectToAction("Index", "Companies");
            }

            var subsidiaries = _context.Subsidiaries.Where(x => x.ComId == company.ComId && x.UserId == user.Id);
            ViewData["ComId"] = new SelectList(companies, "ComId", "ComDescription");
            ViewData["SubId"] = new SelectList(subsidiaries, "SubId", "SubDescription");
            return View();
        }

        [Route("Create")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CatDescription,SubId")] Categories categories)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.GetUserAsync(HttpContext.User);
                categories.CatDate = DateTimeExtension.GetUserDateTime(user.TimeZone);
                categories.CatEnabled = true;
                categories.CatId = await _generator.GenerateIdAsync("CAT", "Register.Categories");
                categories.UserId = user.Id;
                _context.Add(categories);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            FillComSub(categories);

            return View(categories);
        }

        [Route("GetCategories/{subId}")]
        [HttpGet("GetCategories/{subId}")]
        public async Task<JsonResult> GetCategories(string subId)
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;
            var list = await _context.Categories
                  .AsNoTracking()
                  .Where(x => x.CatEnabled == true
                  && x.SubId == subId
                  && x.UserId == user.Id).ToListAsync();

            return Json(list);
        }

        private void FillComSub(Categories categories)
        {
            var sub = _context.Subsidiaries.Where(x => x.SubId == categories.SubId).Include(x => x.Com).FirstOrDefault();
            ViewData["ComId"] = new SelectList(_context.Companies, "ComId", "ComDescription", sub.ComId);
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubDescription", categories.SubId);
        }

        [HttpGet("Edit/{id}")]
        [Route("Edit/{id}")]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(HttpContext.User);

            var categories = await _context.Categories
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.CatId == id && m.CatEnabled == true && m.UserId == user.Id);

            if (categories == null)
            {
                return NotFound();
            }

            if (categories.UserId != user.Id)
            {
                return NotFound();
            }

            FillComSub(categories);

            return View(categories);
        }

        [Route("Edit/{id}")]
        [HttpPost("Edit/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("CatDescription,SubId")] Categories categories)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            var updateModel = await _context.Categories
                .Include(x => x.User)
                .SingleOrDefaultAsync(c => c.CatId == id
                && c.CatEnabled == true && c.UserId == user.Id);

            if (updateModel.UserId != user.Id)
            {
                return NotFound();
            }

            if (id != updateModel.CatId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (await TryUpdateModelAsync<Categories>(updateModel, "", c => c.CatDescription, c => c.SubId))
                {
                    try
                    {
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateException)
                    {

                        if (!CategoriesExists(updateModel.CatId))
                        {
                            return NotFound();
                        }

                        ModelState.AddModelError("", "No se pudo guardar los cambios. " +
           "Intente de nuevo, y si el problema persiste, " +
           "Comuníquese con el administrador del sistema.");

                    }

                    return RedirectToAction(nameof(Index));

                }

                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);

                foreach (var item in allErrors)
                {
                    ModelState.AddModelError("", item.ErrorMessage);
                }

            }

            FillComSub(categories);

            return View(updateModel);
        }

        [HttpGet("Delete/{id}")]
        [Route("Delete/{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(HttpContext.User);

            var categories = await _context.Categories
                .Include(c => c.Sub)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.CatId == id && m.CatEnabled == true && m.UserId == user.Id);

            if (categories == null)
            {
                return NotFound();
            }

            return View(categories);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {

            var categories = await _context.Categories.SingleOrDefaultAsync(m => m.CatId == id && m.CatEnabled == true);

            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (categories.UserId != user.Id)
            {
                return NotFound();
            }

            categories.CatEnabled = false;

            if (await TryUpdateModelAsync<Categories>(categories, "", c => c.CatEnabled))
            {
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {
                    if (!CategoriesExists(categories.CatId))
                    {
                        return NotFound();
                    }

                    ModelState.AddModelError("", "No se pudo eliminar el registro. " +
       "Intente de nuevo, y si el problema persiste, " +
       "Comuníquese con el administrador del sistema.");
                }
                return RedirectToAction(nameof(Index));
            }

            return NotFound();

        }

        private bool CategoriesExists(string id)
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;
            return _context.Categories.AsNoTracking().Any(e => e.CatId == id
            && e.CatEnabled == true && e.UserId == user.Id);
        }
    }
}
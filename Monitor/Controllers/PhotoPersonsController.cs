﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Monitor.Data;
using Monitor.Models;
using Monitor.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Monitor.Extensions.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Authorization;

namespace Monitor.Controllers
{
    [Authorize]
    [Route("PhotoPersons")]
    public class PhotoPersonsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly Generator _generator;
        private readonly UserManager<ApplicationUser> _userManager;
        private IHostingEnvironment _env;
        private readonly Blob_Operations _blob;
        private readonly RecognitionExtensions recognitionExtensions;
        private readonly IConfiguration _iconfiguration;

        public PhotoPersonsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IHostingEnvironment env, IConfiguration iconfiguration)
        {
            _context = context;
            _generator = new Generator(_context);
            _userManager = userManager;
            _env = env;
            _iconfiguration = iconfiguration;
            _blob = new Blob_Operations();
            recognitionExtensions = new RecognitionExtensions(_context);
        }

        [Route("Index/{idPerson}")]
        public async Task<IActionResult> Index(string idPerson)
        {
            if (idPerson == null)
            {
                return NotFound();
            }

            //Get user information
            var user = await _userManager.GetUserAsync(HttpContext.User);

            ViewData["id"] = idPerson;
            var applicationDbContext = _context.PhotoPersons.Include(p => p.Per)
                .Include(p => p.User)
                .Where(x => x.PerId == idPerson && x.Enabled == true && x.UserId == user.Id);
            return View(await applicationDbContext.ToListAsync());
        }

        [Route("Create/{id}")]
        [HttpGet("Create/{id}")]
        public IActionResult Create(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            ViewData["PerId"] = id;
            return View();
        }

        [Route("Create/{id}")]
        [HttpPost("Create/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PerId")] PhotoPerson photoPerson)
        {
            if (ModelState.IsValid)
            {
                //Get user 
                var user = _userManager.GetUserAsync(HttpContext.User).Result;

                var person = _context.Persons.Include(x => x.Sub).Where(x => x.PerId == photoPerson.PerId).FirstOrDefault();

                var photo = HttpContext.Request.Form.Files;

                //Get Azure Storage Account
                string account = _iconfiguration.GetSection("ConnectionStrings").GetSection("StorageConnectionString").Value;

                //Validate if Photo exist
                if (photo.FirstOrDefault() != null)
                {
                    //Update Photo in Blob
                    photoPerson.UrlImage = await _blob.UploadPhotoPerson(photo.FirstOrDefault(), account);

                    //Validate blob photo 
                    if (photoPerson.UrlImage != null)
                    {
                        try
                        {
                            //Create person to recognize

                            var r = await recognitionExtensions.AddPhotoPersonAsync(photoPerson.UrlImage
                                , person, person.Sub.ComId.ToLower(), user);

                            _context.PhotoPersons.Add(r);
                            await _context.SaveChangesAsync();

                            return RedirectToAction("Index", new { id = photoPerson.PerId });
                        }
                        catch (Exception ex)
                        {
                            //Add Error 
                            ModelState.AddModelError("FaceApi", "No se encuentra un rostro.");
                            //Delete blob photo
                            await _blob.DeletePhotoPerson(person.PerPhoto, account);
                            //Show Error to the user.
                            IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                            foreach (var item in allErrors)
                            {
                                ModelState.AddModelError("", item.ErrorMessage);
                            }
                        }
                    }
                }
            }
            ViewData["PerId"] = photoPerson.PerId;
            return View(photoPerson);
        }

        [Route("Delete/{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var photoPerson = await _context.PhotoPersons
                .Include(p => p.Per)
                .Include(p => p.User)
                .SingleOrDefaultAsync(m => m.PhoId == id);
            if (photoPerson == null)
            {
                return NotFound();
            }

            return View(photoPerson);
        }

        [Route("Delete/{id}")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var delete = await _context.PhotoPersons
                .Include(x => x.Per).Include(x => x.Per.Sub)
                .SingleOrDefaultAsync(m => m.PhoId == id);

            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (delete.UserId != user.Id)
            {
                return NotFound();
            }

            delete.Enabled = false;

            if (await TryUpdateModelAsync<PhotoPerson>(delete, "", c => c.Enabled))
            {
                try
                {
                    var r = await recognitionExtensions.DeletePhotoPersonAsync(delete, delete.Per.Sub.ComId.ToLower(), user);

                    if (r == "1")
                    {
                        //await _context.SaveChangesAsync();
                    }
                    else
                    {
                        ModelState.AddModelError("", "No se pudo eliminar.");
                    }

                }
                catch (DbUpdateException)
                {
                    if (!PhotoPersonExists(delete.PerId))
                    {
                        return NotFound();
                    }

                    ModelState.AddModelError("", "No se pudo eliminar el registro. " +
       "Intente de nuevo, y si el problema persiste, " +
       "Comuníquese con el administrador del sistema.");
                }
                return RedirectToAction(nameof(Index), new { id = delete.PerId });
            }

            return NotFound();

        }

        private bool PhotoPersonExists(string id)
        {
            return _context.PhotoPersons.Any(e => e.PhoId == id);
        }
    }
}
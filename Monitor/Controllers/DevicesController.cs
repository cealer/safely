﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Monitor.Data;
using Monitor.Models;
using Monitor.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Monitor.Models.ViewModels;

namespace Monitor.Controllers
{
    [Authorize]
    [Route("Devices")]
    public class DevicesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly Generator _generator;
        private readonly UserManager<ApplicationUser> _userManager;

        public DevicesController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _generator = new Generator(_context);
            _userManager = userManager;
        }

        [HttpGet("Index")]
        [Route("Index")]
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            var applicationDbContext = _context.Devices
                .AsNoTracking()
                .Include(d => d.User).Where(x => x.UserId == user.Id
                && x.DevEnabled == true);

            return View(await applicationDbContext.ToListAsync());
        }

        [HttpGet("Details/{id}")]
        [Route("Details/{id}")]
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(HttpContext.User);

            var devices = await _context.Devices
                .Include(d => d.User)
                .SingleOrDefaultAsync(m => m.DevId == id && m.UserId == user.Id
                && m.DevEnabled == true);

            if (devices == null)
            {
                return NotFound();
            }

            return View(devices);
        }

        [Route("AddDevice")]
        [HttpPost("AddDevice")]
        public async Task<JsonResult> AddDevice(DeviceViewModel deviceViewModel)
        {
            try
            {

                if (deviceViewModel.CodeId != null && deviceViewModel.DevDescription != null)
                {
                    var user = _userManager.GetUserAsync(HttpContext.User).Result;

                    var id = _generator.GenerateIdAsync("DEV", "Register.Devices");

                    var Device = new Devices
                    {
                        CamDate = DateTimeExtension.GetUserDateTime(user.TimeZone),
                        DevEnabled = true,
                        CodeId =deviceViewModel.CodeId,
                        DevDescription =deviceViewModel.DevDescription,
                        UserId = user.Id,
                        DevId = id.Result
                    };

                    _context.Devices.Add(Device);
                    await _context.SaveChangesAsync();
                    return Json(Device);
                }
            }
            catch (Exception)
            {
                return Json(null);
            }
            return Json(null);
        }

        //public IActionResult Create()
        //{
        //    return View();
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("DevDescription,CodeId")] Devices devices)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var user = await _userManager.GetUserAsync(HttpContext.User);
        //        devices.UserId = user.Id;
        //        devices.CamDate = DateTimeExtension.GetUserDateTime(user.TimeZone);
        //        devices.DevEnabled = true;
        //        devices.DevId = await _generator.GenerateIdAsync("Register", "Devices");
        //        _context.Add(devices);
        //        await _context.SaveChangesAsync();
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(devices);
        //}

        [Route("Edit/{id}")]
        [HttpGet("Edit/{id}")]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(HttpContext.User);

            var devices = await _context.Devices.SingleOrDefaultAsync(m => m.DevId == id
            && m.UserId == user.Id
           && m.DevEnabled == true);

            if (devices == null)
            {
                return NotFound();
            }

            return View(devices);
        }

        [Route("Edit/{id}")]
        [HttpPost("Edit/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, Devices devices)
        {
            if (id != devices.DevId)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(HttpContext.User);

            var updateModel = await _context.Devices
                .Include(x => x.User)
                .SingleOrDefaultAsync(c => c.DevId == id
                && c.DevEnabled == true && c.UserId == user.Id);

            if (updateModel.UserId != user.Id)
            {
                return NotFound();
            }

            if (id != updateModel.DevId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (await TryUpdateModelAsync<Devices>(updateModel, "", c => c.DevDescription))
                {
                    try
                    {
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateException)
                    {
                        if (!DevicesExists(updateModel.DevId))
                        {
                            return NotFound();
                        }

                        ModelState.AddModelError("", "No se pudo guardar los cambios. " +
           "Intente de nuevo, y si el problema persiste, " +
           "Comuníquese con el administrador del sistema.");
                    }

                    return RedirectToAction(nameof(Index));
                }

                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);

                foreach (var item in allErrors)
                {
                    ModelState.AddModelError("", item.ErrorMessage);
                }

            }

            return View(updateModel);

        }

        [Route("Delete/{id}")]
        [HttpGet("Delete/{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(HttpContext.User);

            var devices = await _context.Devices
                .Include(d => d.User)
                .SingleOrDefaultAsync(m => m.DevId == id
                && m.DevEnabled == true && m.UserId == user.Id);

            if (devices == null)
            {
                return NotFound();
            }

            return View(devices);
        }

        [HttpPost("Delete/{id}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            var devices = await _context.Devices.SingleOrDefaultAsync(
                m => m.DevId == id
                && m.DevEnabled == true
                && m.UserId == user.Id);

            if (devices.UserId != user.Id)
            {
                return NotFound();
            }

            devices.DevEnabled = false;

            if (await TryUpdateModelAsync<Devices>(devices, "", c => c.DevEnabled))
            {
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {
                    if (!DevicesExists(devices.DevId))
                    {
                        return NotFound();
                    }

                    ModelState.AddModelError("", "No se pudo eliminar el registro. " +
       "Intente de nuevo, y si el problema persiste, " +
       "Comuníquese con el administrador del sistema.");
                }
                return RedirectToAction(nameof(Index));
            }

            return NotFound();
        }

        private bool DevicesExists(string id)
        {
            return _context.Devices.AsNoTracking().Any(
                e => e.DevId == id && e.DevEnabled == true);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Monitor.Models;
using Microsoft.AspNetCore.Authorization;
using Monitor.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Monitor.Models.ViewModels;

namespace Monitor.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public HomeController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult Index() => View();

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        [Authorize]
        public async Task<IActionResult> Dashboard()
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            var dashboard = new DashBoardViewModels();

            if (user != null)
            {

                dashboard.recs = await _context.Recognitions.
                     Include(x => x.Cam).
                     Where(x => x.Cam.UserId == user.Id).CountAsync();

                dashboard.people = await _context.Persons.
                    Where(x => x.UserId == user.Id).CountAsync();

                dashboard.camaras = await _context.Camaras.
                    Where(x => x.UserId == user.Id).CountAsync();

                dashboard.alarms = await _context.Alarms.
                    Where(x => x.UserId == user.Id).CountAsync();

                dashboard.LastAlarms = await _context.Alarms.
                    Include(x=>x.Per).
                    Where(x => x.UserId == user.Id).ToListAsync();

                dashboard.listRecs = await _context.Recognitions
                  .Include(x => x.Cam)
                  .Include(x => x.Sub)
                  .Include(x => x.Per)
                  .Include(x => x.Sub.Com).
                  Include(x => x.DetailsRecognitions)
                  .OrderByDescending(x => x.RecId).
                  Where(x => x.Cam.UserId == user.Id)
                  .Select(x => new RecViewModel
                  {
                      Person = x.Per.FullName,
                      Photo = x.RecPath,
                      Com = x.Sub.Com.ComDescription,
                      Sub = x.Sub.SubDescription,
                      Cam = x.Cam.CamDescription,
                      DateRegister = x.RecDate
                  })
              .Take(4).ToListAsync();

            }

            return View(dashboard);
        }

        public IActionResult Fallback() => View();

        public IActionResult Error() => View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Monitor.Data;
using Monitor.Model;
using Monitor.Extensions;
using FCM.Net;
using Microsoft.AspNetCore.Authorization;
using Monitor.Models;
using Microsoft.AspNetCore.Identity;
using Monitor.Models.ViewModels;
using System.Globalization;

namespace Monitor.Controllers
{
    [Authorize]
    [Route("Recognitions")]
    public class RecognitionsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly Generator generator;
        private readonly RecognitionExtensions recognitionExtensions;
        private readonly UserManager<ApplicationUser> _userManager;

        public RecognitionsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            generator = new Generator(_context);
            recognitionExtensions = new RecognitionExtensions(_context);
            _userManager = userManager;
        }

        [Route("Index")]
        public async Task<IActionResult> Index()
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            var applicationDbContext = _context.Recognitions.AsNoTracking().Include(r => r.Cam)
                .Include(r => r.Per)
                .Include(r => r.Sub)
                .Include(r => r.Cam).Where(x => x.Cam.UserId == user.Id)
                .OrderByDescending(x => x.RecId);

            return View(await applicationDbContext.ToListAsync());
        }

        [Route("Details/{id}")]
        [HttpGet("Details/{id}")]
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var recognitions = await _context.Recognitions
                .Include(r => r.Cam)
                .Include(r => r.Per)
                .Include(r => r.Sub)
                .SingleOrDefaultAsync(m => m.RecId == id);
            if (recognitions == null)
            {
                return NotFound();
            }

            return View(recognitions);
        }

        [Route("Test")]
        [HttpPost]
        public async Task<IActionResult> Test()
        {
            var registrationId = "ctmdSsSs4Ag:APA91bHNKieSC4meMy0lZbR2vLKHvPQHBF0AxQGYLOCJp05x27jnJu7LHcChsxWm91hb81vnzH4p6t_MQOGfTs3HmuI65i62gLdD1APKBNCrpg00Rd36qK_xipzNCtS4ToTBk9jo9J_Q";

            //You can get the server Key by accessing the url/ Você pode obter a chave do servidor acessando a url 
            //https://console.firebase.google.com/project/MY_PROJECT/settings/cloudmessaging";
            using (var sender = new Sender("AAAAcPbuCu4:APA91bFIIDkuXGdduhY1YlxmnhcKRa9fXa4ilj8qPx_99Tgeq7k4zaMfMAi_5Lpk9fu8u739FvmyvXcFbJ6Vo3enx6Rl2G5WhY3DFzPSpXlm6o-igpbL4M9XJbPauwbnYZrC9cTFY1pD"))
            {
                var message = new Message
                {
                    RegistrationIds = new List<string> { registrationId },
                    Notification = new Notification
                    {
                        Title = "Test from FCM.Net",
                        Body = $"Hello World@!{DateTime.Now.ToString()}"
                    }
                };
                var result = await sender.SendAsync(message);
                Console.WriteLine($"Success: {result.MessageResponse.Success}");

                var json = "{\"notification\":{\"title\":\"json message\",\"body\":\"works like a charm!\"},\"to\":\"" + registrationId + "\"}";
                result = await sender.SendAsync(json);
                Console.WriteLine($"Success: {result.MessageResponse.Success}");
            }

            //await recognitionExtensions.DetectAlarm("PER000000000000001");
            //var model = new NotificationResponse
            //{
            //    notification = new Notification
            //    {
            //        body = "Se disparado una alarmas",
            //        click_action = "https://localhost:44384",
            //        title = "Desconocido"
            //    }
            //    ,
            //    to = @"ctmdSsSs4Ag:APA91bHNKieSC4meMy0lZbR2vLKHvPQHBF0AxQGYLOCJp05x27jnJu7LHcChsxWm91hb81vnzH4p6t_MQOGfTs3HmuI65i62gLdD1APKBNCrpg00Rd36qK_xipzNCtS4ToTBk9jo9J_Q"
            //};

            //var request = JsonConvert.SerializeObject(model);
            //var content = new StringContent(request, Encoding.UTF8, "application/json");
            //var client = new HttpClient();
            //client.DefaultRequestHeaders.Add();
            //client.DefaultRequestHeaders.Add(string.Format("Authorization: key={0}", "AAAAcPbuCu4:APA91bFIIDkuXGdduhY1YlxmnhcKRa9fXa4ilj8qPx_99Tgeq7k4zaMfMAi_5Lpk9fu8u739FvmyvXcFbJ6Vo3enx6Rl2G5WhY3DFzPSpXlm6o-igpbL4M9XJbPauwbnYZrC9cTFY1pD"));
            //client.BaseAddress = new Uri("https://fcm.googleapis.com");
            //var url = string.Format("/fcm/send");
            //var response = await client.PostAsync(url, content);

            //if (!response.IsSuccessStatusCode)
            //{
            //    return Ok(new Response
            //    {
            //        IsSuccess = false,
            //        Message = response.StatusCode.ToString(),
            //    });
            //}

            //var result = await response.Content.ReadAsStringAsync();

            return Ok();
        }

        [AllowAnonymous]
        [Route("GetUnknow")]
        public async Task<JsonResult> GetUnknow()
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            var description = new RecognitionsViewModels();

            description.Recognition = await _context.Recognitions
                .Include(x => x.Cam)
                .Include(x => x.Sub)
                .Include(x => x.Per)
                .Include(x => x.Sub.Com)
                .OrderByDescending(x => x.RecId).
                Where(x => x.Cam.UserId == user.Id).FirstOrDefaultAsync();

            description.Company = description.Recognition.Sub.Com;
            description.Camara = description.Recognition.Cam;
            description.Person = description.Recognition.Per;
            description.Subsidiary = description.Recognition.Sub;

            description.Descriptions = await _context.DescriptionRec.Where(x => x.RecId == description.Recognition.RecId).ToListAsync();

            return Json(description);
        }

        [AllowAnonymous]
        [Route("GetPerson/{id}/{person}")]
        public async Task<JsonResult> GetUnknow(string id, string person)
        {
            var user = await _context.Users.Where(x => x.Id == id).FirstOrDefaultAsync();

            var description = new RecognitionsViewModels();

            if (user != null)
            {
                description.Recognition = await _context.Recognitions
                    .Include(x => x.Cam)
                    .Include(x => x.Sub)
                    .Include(x => x.Per)
                    .Include(x => x.Sub.Com)
                    .OrderByDescending(x => x.RecId).
                    Where(x => x.Per.FullName.ToLower().Contains(person.ToLower()) &&
                    x.Cam.UserId == user.Id).FirstOrDefaultAsync();

                if (description.Recognition != null)
                {
                    description.Company = description.Recognition.Sub.Com;
                    description.Camara = description.Recognition.Cam;
                    description.Person = description.Recognition.Per;
                    description.Subsidiary = description.Recognition.Sub;
                    description.Descriptions = await _context.DescriptionRec.Where(x => x.RecId == description.Recognition.RecId).ToListAsync();
                }
            }

            return Json(description);
        }

        [AllowAnonymous]
        [Route("GetRec/{id}")]
        public async Task<JsonResult> GetRec(string id)
        {
            var user = await _context.Users.Where(x => x.Id == id).FirstOrDefaultAsync();

            var recList = new List<Recognitions>();

            if (user != null)
            {
                recList = await _context.Recognitions
                    .Include(x => x.Cam)
                    .Include(x => x.Sub)
                    .Include(x => x.Per)
                    .Include(x => x.Sub.Com).
                    Include(x => x.DetailsRecognitions)
                    .OrderByDescending(x => x.RecId).
                    Where(x => x.Cam.UserId == user.Id).ToListAsync();
            }

            return Json(recList);
        }

        [Route("GetRecChart")]
        public async Task<JsonResult> GetRecChart()
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            if (user != null)
            {
                var qry = from cust in await _context.Recognitions.Include(x => x.Per).Include(x => x.Cam).ToListAsync()
                          where cust.Cam.UserId == user.Id
                          group cust by cust.Per.FullName
                into grp
                          select new
                          {
                              Person = grp.Key,
                              Count = grp.Select(x => x.RecId).Distinct().Count()
                          };

                return Json(qry);
            }

            return Json(null);
        }

        [Route("GetRecBar")]
        public async Task<JsonResult> GetRecBar()
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            if (user != null)
            {
                var qry = from cust in await _context.Recognitions.
                          Include(x => x.Per).
                          Include(x => x.Cam).
                          ToListAsync()
                          where cust.Cam.UserId == user.Id
                          group cust by cust.RecDate.ToString("MMMM", CultureInfo.CreateSpecificCulture("es"))
                          into grp
                          select new
                          {
                              Month = grp.Key,
                              Count = grp.Select(x => x.RecId).Distinct().Count()
                          };

                return Json(qry);
            }

            return Json(null);
        }

        [Route("GetLastAlarms")]
        public async Task<JsonResult> GetLastAlarms()
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            if (user != null)
            {
                var List = await _context.Alarms
                    .Where(x => x.UserId == user.Id)
                    .OrderByDescending(x => x.AlmId)
                .Take(4).ToListAsync();

                return Json(List);
            }

            return Json(null);
        }

        [Route("GetPie")]
        public async Task<JsonResult> GetPie()
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            if (user != null)
            {
                var qry =
                    from cust in await _context.Persons
                    .ToListAsync()
                    where cust.UserId == user.Id
                    group cust by cust.PerGender
                    into grp
                    select new
                    {
                        Gender = grp.Key,
                        Count = grp.Select(x => x.PerId).Distinct().Count()
                    };

                return Json(qry);
            }

            return Json(null);
        }

        //[Route("GetStatics")]
        //public async Task<JsonResult> GetStatics()
        //{
        //    var user = _userManager.GetUserAsync(HttpContext.User).Result;

        //    if (user != null)
        //    {
        //        var recs = await _context.Recognitions.
        //            Include(x => x.Cam).
        //            Where(x => x.Cam.UserId == user.Id).CountAsync();

        //        var people = await _context.Persons.
        //            Where(x => x.UserId == user.Id).CountAsync();

        //        var camaras = await _context.Camaras.
        //            Where(x => x.UserId == user.Id).CountAsync();

        //        var alarms = await _context.Alarms.
        //            Where(x => x.UserId == user.Id).CountAsync();

        //        var r = new { recs, people, camaras, alarms };

        //        return Json(r);
        //    }

        //    return Json(null);
        //}

        //    [Route("GetLastRec")]
        //    public async Task<JsonResult> GetLastRec()
        //    {
        //        var user = _userManager.GetUserAsync(HttpContext.User).Result;

        //        if (user != null)
        //        {
        //            var recList = await _context.Recognitions
        //                .Include(x => x.Cam)
        //                .Include(x => x.Sub)
        //                .Include(x => x.Per)
        //                .Include(x => x.Sub.Com).
        //                Include(x => x.DetailsRecognitions)
        //                .OrderByDescending(x => x.RecId).
        //                Where(x => x.Cam.UserId == user.Id)
        //.Select(x => new
        //{
        //    x.Per.FullName,
        //    x.RecPath,
        //    x.Sub.Com.ComDescription,
        //    x.Sub.SubDescription,
        //    x.Cam.CamDescription,
        //    x.RecDate
        //})
        //            .Take(4).ToListAsync();

        //            return Json(recList);
        //        }

        //        return Json(null);
        //    }

        [AllowAnonymous]
        [Route("SaveRecognition")]
        [HttpPost("SaveRecognition")]
        public async Task<IActionResult> PostRecognitions([FromBody] Recognitions recognitions)
        {
            try
            {
                //Recognize Person

                ModelState.Clear();

                recognitions.RecId = await generator.GenerateIdAsync("REC", "Detection.Recognitions");

                var camara = _context.Camaras.Find(recognitions.CamId);

                recognitions.RecDate = DateTimeExtension.GetUserDateTime(camara.TimeZone);
                recognitions.SubId = _context.Camaras.FindAsync(recognitions.CamId).Result.SubId;
                recognitions.RecEnabled = true;
                var company = _context.Subsidiaries.Where(x => x.SubId == recognitions.SubId).FirstOrDefault();

                var rec = await recognitionExtensions.IdentifyPerson(recognitions.RecPath, company.ComId.ToLower());

                recognitions.PerId = rec;

                TryValidateModel(recognitions);

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                _context.Recognitions.Add(recognitions);

                await _context.SaveChangesAsync();

                await SendNotifications(recognitions, camara.UserId);

                await recognitionExtensions.DescribeImage(recognitions.RecPathFull, recognitions.RecId);

                return CreatedAtAction("GetRecognitions", new { id = recognitions.RecId }, recognitions);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        private async Task SendNotifications(Recognitions recognitions, string UserId)
        {
            var Devices = _context.Devices.Where(x => x.DevEnabled == true && x.UserId == UserId);

            foreach (var item in Devices)
            {
                await recognitionExtensions.DetectAlarm(recognitions.PerId,
                recognitions.RecPath, item.CodeId, recognitions.RecId);
            }
        }

        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var recognitions = await _context.Recognitions.SingleOrDefaultAsync(m => m.RecId == id);
            if (recognitions == null)
            {
                return NotFound();
            }
            ViewData["CamId"] = new SelectList(_context.Camaras, "CamId", "CamId", recognitions.CamId);
            ViewData["PerId"] = new SelectList(_context.Persons, "PerId", "PerId", recognitions.PerId);
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId", recognitions.SubId);
            return View(recognitions);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("RecId,PerId,RecPath,RecDate,SubId,CamId,RecEnabled,RecPathFull")] Recognitions recognitions)
        {
            if (id != recognitions.RecId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(recognitions);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RecognitionsExists(recognitions.RecId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CamId"] = new SelectList(_context.Camaras, "CamId", "CamId", recognitions.CamId);
            ViewData["PerId"] = new SelectList(_context.Persons, "PerId", "PerId", recognitions.PerId);
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId", recognitions.SubId);
            return View(recognitions);
        }

        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var recognitions = await _context.Recognitions
                .Include(r => r.Cam)
                .Include(r => r.Per)
                .Include(r => r.Sub)
                .SingleOrDefaultAsync(m => m.RecId == id);
            if (recognitions == null)
            {
                return NotFound();
            }

            return View(recognitions);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var recognitions = await _context.Recognitions.SingleOrDefaultAsync(m => m.RecId == id);
            _context.Recognitions.Remove(recognitions);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RecognitionsExists(string id)
        {
            return _context.Recognitions.Any(e => e.RecId == id);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Monitor.Model;
using Monitor.Data;
using Monitor.Models;
using Monitor.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Monitor.Extensions.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Monitor.Controllers
{
    [Authorize]
    public class PeopleController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly Generator _generator;
        private readonly UserManager<ApplicationUser> _userManager;
        private IHostingEnvironment _env;
        private readonly Blob_Operations _blob;
        private readonly RecognitionExtensions recognitionExtensions;
        private readonly Helper_Gender _helperGender;
        private readonly Blob_Operations blob = new Blob_Operations();

        private readonly IConfiguration _iconfiguration;

        public PeopleController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IHostingEnvironment env, IConfiguration iconfiguration)
        {
            _context = context;
            _generator = new Generator(_context);
            _userManager = userManager;
            _env = env;
            _iconfiguration = iconfiguration;
            _blob = new Blob_Operations();
            recognitionExtensions = new RecognitionExtensions(_context);
            _helperGender = new Helper_Gender();
        }

        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var monitorContext = _context.Persons
                .AsNoTracking().Include(p => p.Cat).Include(x=>x.User)
                .Include(p => p.Sub).Where(x => x.PerEnabled == true
                && x.UserId == user.Id);
            return View(await monitorContext.ToListAsync());
        }

        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var persons = await _context.Persons
                .Include(p => p.Cat)
                .Include(p => p.Sub)
                .SingleOrDefaultAsync(m => m.PerId == id);
            if (persons == null)
            {
                return NotFound();
            }

            return View(persons);
        }

        public IActionResult Create()
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            var companies = _context.Subsidiaries.Where(x => x.SubEnabled == true && x.UserId == user.Id).Select(x => x.Com).Distinct();

            var company = companies.FirstOrDefault();

            if (company == null)
            {
                return RedirectToAction("Index", "Companies");
            }

            var subsidiaries = _context.Subsidiaries.Where(x => x.ComId == company.ComId && x.UserId == user.Id);

            ViewData["ComId"] = new SelectList(companies, "ComId", "ComDescription");
            ViewData["SubId"] = new SelectList(subsidiaries, "SubId", "SubDescription");
            ViewData["PerGender"] = new SelectList(_helperGender.Gender(), "Value", "Description");

            //Llenar Categorias cuando se escoja subarea
            ViewData["CatId"] = new SelectList(_context.Categories
                .Where(x => x.CatEnabled == true
                && x.UserId == user.Id && x.SubId == subsidiaries.FirstOrDefault().SubId)
                , "CatId", "CatDescription");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PerName,PerLastName,IdentificationDocument,PerEmail,PerPhone,PerDate,PerGender,PerDateBirth,SubId,CatId,PerEnabled,PerPhoto")] People person)
        {
            //Get user 
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            if (ModelState.IsValid)
            {

                string Photo = null;
                person.PerDate = DateTimeExtension.GetUserDateTime(user.TimeZone);
                person.PerEnabled = true;
                person.PerId = await _generator.GenerateIdAsync("PER", "Register.Persons");
                person.UserId = user.Id;

                var photo = HttpContext.Request.Form.Files;

                //Get Azure Storage Account
                string account = _iconfiguration.GetSection("ConnectionStrings").GetSection("StorageConnectionString").Value;

                //Validate if Photo exist
                if (photo.FirstOrDefault() != null)
                {
                    //Update Photo in Blob
                    Photo = await _blob.UploadPhotoPerson(photo.FirstOrDefault(), account);

                    //Set Blob Photo
                    person.PerPhoto = Photo;

                    var company = _context.Subsidiaries.Where(x => x.SubId == person.SubId).FirstOrDefault();

                    //Validate blob photo 
                    if (photo != null)
                    {

                        try
                        {
                            //Create person to recognize

                            var r = await recognitionExtensions.CreatePersonAsync(Photo, person, company.ComId.ToLower(), user);

                            person.PerPath = r.personIdApi;

                            _context.Add(person);

                            await _context.SaveChangesAsync();

                            _context.PhotoPersons.Add(r);
                            await _context.SaveChangesAsync();

                            return RedirectToAction("Index");
                        }
                        catch (Exception ex)
                        {
                            //Add Error 
                            ModelState.AddModelError("FaceApi", "No se encuentra un rostro.");
                            //Delete blob photo
                            await _blob.DeletePhotoPerson(person.PerPhoto, account);
                            //Show Error to the user.
                            IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                            foreach (var item in allErrors)
                            {
                                ModelState.AddModelError("", item.ErrorMessage);
                            }
                        }
                    }
                }
                else
                {
                    _context.Add(person);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }

            ViewData["PerGender"] = new SelectList(_helperGender.Gender(), "Value", "Description");

            FillComSub(person, user.Id);

            return View(person);
        }

        private void FillComSub(People people, string UserId)
        {
            var sub = _context.Subsidiaries.Where(x => x.SubId == people.SubId).Include(x => x.Com).FirstOrDefault();
            ViewData["ComId"] = new SelectList(_context.Companies.Where(x => x.UserId == UserId
            && x.ComEnabled == true), "ComId", "ComDescription", sub.ComId);
            ViewData["SubId"] = new SelectList(_context.Subsidiaries.
                Where(x => x.UserId == UserId
                && x.SubId == people.SubId
                && x.SubEnabled == true), "SubId", "SubDescription", people.SubId);
            ViewData["CatId"] = new SelectList(_context.Categories.Where(x => x.UserId == UserId
            && x.SubId == sub.SubId), "CatId", "CatDescription", people.CatId);

        }

        public async Task<IActionResult> Edit(string id)
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;

            if (id == null)
            {
                return NotFound();
            }

            var persons = await _context.Persons.SingleOrDefaultAsync(m => m.PerId == id);
            if (persons == null)
            {
                return NotFound();
            }
            ViewData["PerGender"] = new SelectList(_helperGender.Gender(), "Value", "Description", "Seleccionar sexo");
            FillComSub(persons, user.Id);
            return View(persons);
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPost(string id)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            var updateModel = await _context.Persons
                .Include(x => x.User)
                .SingleOrDefaultAsync(c => c.PerId == id);

            if (updateModel.UserId != user.Id)
            {
                return NotFound();
            }

            if (id != updateModel.PerId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (await TryUpdateModelAsync<People>(updateModel, "", c => c.PerName,
                    c => c.PerDateBirth,
                    c => c.PerEmail,
                    c => c.PerGender,
                    c => c.PerLastName,
                    c => c.PerPhone,
                    c => c.SubId,
                    c => c.IdentificationDocument,
                    c => c.CatId
                    ))
                {
                    try
                    {
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateException)
                    {
                        if (!PersonsExists(updateModel.PerId))
                        {
                            return NotFound();
                        }

                        ModelState.AddModelError("", "No se pudo guardar los cambios. " +
           "Intente de nuevo, y si el problema persiste, " +
           "Comuníquese con el administrador del sistema.");
                    }
                    return RedirectToAction(nameof(Index));
                }

                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);

                foreach (var item in allErrors)
                {
                    ModelState.AddModelError("", item.ErrorMessage);
                }

            }
            ViewData["PerGender"] = new SelectList(_helperGender.Gender(), "Value", "Description", "Seleccionar sexo", updateModel.PerGender);
            ViewData["CatId"] = new SelectList(_context.Categories, "CatId", "CatDescription", updateModel.CatId);
            FillComSub(updateModel, user.Id);
            return View(updateModel);
        }

        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(HttpContext.User);

            var persons = await _context.Persons
                .Include(p => p.Cat)
                .Include(p => p.Sub)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.PerId == id && m.PerEnabled == true && m.UserId == user.Id);

            if (persons == null)
            {
                return NotFound();
            }

            return View(persons);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var people = await _context.Persons.SingleOrDefaultAsync(m => m.PerId == id);

            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (people.UserId != user.Id)
            {
                return NotFound();
            }

            people.PerEnabled = false;

            if (await TryUpdateModelAsync<People>(people, "", c => c.PerEnabled))
            {
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {
                    if (!PersonsExists(people.PerId))
                    {
                        return NotFound();
                    }

                    ModelState.AddModelError("", "No se pudo eliminar el registro. " +
       "Intente de nuevo, y si el problema persiste, " +
       "Comuníquese con el administrador del sistema.");
                }
                return RedirectToAction(nameof(Index));
            }

            return NotFound();
        }

        [Route("People/GetPeople/{subId}")]
        [HttpGet("People/GetPeople/{subId}")]
        public async Task<JsonResult> GetPeople(string subId)
        {
            var user = _userManager.GetUserAsync(HttpContext.User).Result;
            var list = await _context.Persons
                  .AsNoTracking()
                  .Where(x => x.PerEnabled == true
                  && x.SubId == subId
                  && x.UserId == user.Id).ToListAsync();

            return Json(list);
        }

        private bool PersonsExists(string id)
        {
            return _context.Persons.AsNoTracking().Any(e => e.PerId == id);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Monitor.Models;
using Monitor.Model;

namespace Monitor.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public virtual DbSet<Alarms> Alarms { get; set; }
        public virtual DbSet<Camaras> Camaras { get; set; }
        public virtual DbSet<Categories> Categories { get; set; }
        public virtual DbSet<Companies> Companies { get; set; }
        public virtual DbSet<DescriptionRec> DescriptionRec { get; set; }
        public virtual DbSet<DetailsAlarms> DetailsAlarms { get; set; }
        public virtual DbSet<DetailsRecognitions> DetailsRecognitions { get; set; }
        public virtual DbSet<HistoryTables> HistoryTables { get; set; }
        public virtual DbSet<NeuralNetworks> NeuralNetworks { get; set; }
        public virtual DbSet<People> Persons { get; set; }
        public virtual DbSet<Recognitions> Recognitions { get; set; }
        public virtual DbSet<Subsidiaries> Subsidiaries { get; set; }
        public virtual DbSet<Training> Training { get; set; }
        public virtual DbSet<Xobjects> Xobjects { get; set; }
        public virtual DbSet<Devices> Devices { get; set; }
        public virtual DbSet<PhotoPerson> PhotoPersons { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity<Alarms>(entity =>
            {
                //entity.HasIndex(e => e.AlmDescription)
                //    .HasName("Alm_Description_uq")
                //    .IsUnique();

                entity.Property(e => e.AlmId).ValueGeneratedNever();

                entity.HasOne(d => d.Obj)
                    .WithMany(p => p.Alarms)
                    .HasForeignKey(d => d.ObjId)
                    .HasConstraintName("FK_Objects_Alarms");

                entity.HasOne(d => d.Per)
                    .WithMany(p => p.Alarms)
                    .HasForeignKey(d => d.PerId)
                    .HasConstraintName("FK_Persons_Alarms");

                entity.HasOne(d => d.Sub)
                    .WithMany(p => p.Alarms)
                    .HasForeignKey(d => d.SubId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Subsidiaries_Alarms");
            });

            builder.Entity<Camaras>(entity =>
            {
                //entity.HasIndex(e => e.CamSource)
                //    .HasName("Cam_Source_uq")
                //    .IsUnique();

                entity.Property(e => e.CamId).ValueGeneratedNever();

                entity.Property(e => e.CamDescription).IsUnicode(false);

                entity.HasOne(d => d.Cam)
                    .WithOne(p => p.InverseCam)
                    .HasForeignKey<Camaras>(d => d.CamId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Camaras_Camaras");

                entity.HasOne(d => d.Sub)
                    .WithMany(p => p.Camaras)
                    .HasForeignKey(d => d.SubId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Subsidiaries_Camaras");
            });

            builder.Entity<Categories>(entity =>
            {
                entity.Property(e => e.CatId).ValueGeneratedNever();

                entity.Property(e => e.CatDescription).IsUnicode(false);

                entity.HasOne(d => d.Sub)
                    .WithMany(p => p.Categories)
                    .HasForeignKey(d => d.SubId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Subsidiaries_Categories");
            });

            builder.Entity<Companies>(entity =>
            {
                entity.Property(e => e.ComId).ValueGeneratedNever();
            });

            builder.Entity<DescriptionRec>(entity =>
            {
                entity.HasIndex(e => e.RecId);

                entity.Property(e => e.DesId).ValueGeneratedNever();
            });

            builder.Entity<DetailsAlarms>(entity =>
            {
                entity.Property(e => e.DalId).ValueGeneratedNever();

                entity.HasOne(d => d.Alm)
                    .WithMany(p => p.DetailsAlarms)
                    .HasForeignKey(d => d.AlmId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Alarms_DetailsAlarms");

                entity.HasOne(d => d.Sub)
                    .WithMany(p => p.DetailsAlarms)
                    .HasForeignKey(d => d.SubId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_37");
            });

            builder.Entity<DetailsRecognitions>(entity =>
            {
                entity.HasIndex(e => e.AlmId);

                entity.Property(e => e.DreId).ValueGeneratedNever();

                entity.HasOne(d => d.Obj)
                    .WithMany(p => p.DetailsRecognitions)
                    .HasForeignKey(d => d.ObjId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Objects_DetailRecognitions");

                entity.HasOne(d => d.Rec)
                    .WithMany(p => p.DetailsRecognitions)
                    .HasForeignKey(d => d.RecId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Recognitions_DetailRecognitions");
            });

            builder.Entity<HistoryTables>(entity =>
            {
                entity.Property(e => e.HisId).ValueGeneratedNever();

                entity.Property(e => e.HisOriginal).IsUnicode(false);

                entity.Property(e => e.HisTable).IsUnicode(false);
            });

            builder.Entity<NeuralNetworks>(entity =>
            {
                //entity.HasIndex(e => e.NeuDescription)
                //    .HasName("Neu_Description_uq")
                //    .IsUnique();

                entity.Property(e => e.NeuId).ValueGeneratedNever();

                entity.HasOne(d => d.Sub)
                    .WithMany(p => p.NeuralNetworks)
                    .HasForeignKey(d => d.SubId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Subsidiaries_NeuralNetworks");
            });

            builder.Entity<People>(entity =>
            {
                //entity.HasIndex(e => e.PerDni)
                //    .HasName("Per_Dni_uq")
                //    .IsUnique();

                //entity.HasIndex(e => e.PerEmail)
                //    .HasName("Per_Email_uq")
                //    .IsUnique();

                entity.HasIndex(e => e.PerPath)
                    .HasName("Per_Path")
                    .IsUnique();

                entity.Property(e => e.PerId).ValueGeneratedNever();

                entity.HasOne(d => d.Cat)
                    .WithMany(p => p.Persons)
                    .HasForeignKey(d => d.CatId)
                    .HasConstraintName("FK_Category_Persons");

                entity.HasOne(d => d.Sub)
                    .WithMany(p => p.Persons)
                    .HasForeignKey(d => d.SubId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Subsidiaries_Persons");
            });

            builder.Entity<Recognitions>(entity =>
            {
                entity.Property(e => e.RecId).ValueGeneratedNever();

                entity.HasOne(d => d.Cam)
                    .WithMany(p => p.Recognitions)
                    .HasForeignKey(d => d.CamId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Camaras_Recognitions");

                entity.HasOne(d => d.Per)
                    .WithMany(p => p.Recognitions)
                    .HasForeignKey(d => d.PerId)
                    .HasConstraintName("FK_Persons_Recognitions");

                entity.HasOne(d => d.Sub)
                    .WithMany(p => p.Recognitions)
                    .HasForeignKey(d => d.SubId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Subsidiaries_Recognitions");
            });

            builder.Entity<Subsidiaries>(entity =>
            {
                //entity.HasIndex(e => e.SubDescription)
                //    .HasName("Sub_Description_uq")
                //    .IsUnique();

                entity.Property(e => e.SubId).ValueGeneratedNever();

                entity.Property(e => e.SubDescription).IsUnicode(false);
            });

            builder.Entity<Training>(entity =>
            {
                //entity.HasIndex(e => e.TraDescription)
                //    .HasName("Tra_Description_uq")
                //    .IsUnique();

                entity.Property(e => e.TraId).ValueGeneratedNever();

                entity.Property(e => e.TraDescription).IsUnicode(false);

                entity.HasOne(d => d.Neu)
                    .WithMany(p => p.Training)
                    .HasForeignKey(d => d.NeuId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NeuralNetworks_Training");
            });

            builder.Entity<Xobjects>(entity =>
            {
                entity.Property(e => e.ObjId).ValueGeneratedNever();

                entity.Property(e => e.ObjDescription).IsUnicode(false);

                entity.HasOne(d => d.Sub)
                    .WithMany(p => p.Xobjects)
                    .HasForeignKey(d => d.SubId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Subsidiaries_Objects");
            });

        }
    }
}
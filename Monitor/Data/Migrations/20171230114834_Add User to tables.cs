﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Monitor.Data.Migrations
{
    public partial class AddUsertotables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Usr_Id",
                schema: "Register",
                table: "XObjects",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Usr_Id",
                schema: "Register",
                table: "Persons",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Usr_Id",
                schema: "Register",
                table: "Categories",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Usr_Id",
                schema: "Register",
                table: "Camaras",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_XObjects_Usr_Id",
                schema: "Register",
                table: "XObjects",
                column: "Usr_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Persons_Usr_Id",
                schema: "Register",
                table: "Persons",
                column: "Usr_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Categories_Usr_Id",
                schema: "Register",
                table: "Categories",
                column: "Usr_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Camaras_Usr_Id",
                schema: "Register",
                table: "Camaras",
                column: "Usr_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Camaras_AspNetUsers_Usr_Id",
                schema: "Register",
                table: "Camaras",
                column: "Usr_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Categories_AspNetUsers_Usr_Id",
                schema: "Register",
                table: "Categories",
                column: "Usr_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Persons_AspNetUsers_Usr_Id",
                schema: "Register",
                table: "Persons",
                column: "Usr_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_XObjects_AspNetUsers_Usr_Id",
                schema: "Register",
                table: "XObjects",
                column: "Usr_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Camaras_AspNetUsers_Usr_Id",
                schema: "Register",
                table: "Camaras");

            migrationBuilder.DropForeignKey(
                name: "FK_Categories_AspNetUsers_Usr_Id",
                schema: "Register",
                table: "Categories");

            migrationBuilder.DropForeignKey(
                name: "FK_Persons_AspNetUsers_Usr_Id",
                schema: "Register",
                table: "Persons");

            migrationBuilder.DropForeignKey(
                name: "FK_XObjects_AspNetUsers_Usr_Id",
                schema: "Register",
                table: "XObjects");

            migrationBuilder.DropIndex(
                name: "IX_XObjects_Usr_Id",
                schema: "Register",
                table: "XObjects");

            migrationBuilder.DropIndex(
                name: "IX_Persons_Usr_Id",
                schema: "Register",
                table: "Persons");

            migrationBuilder.DropIndex(
                name: "IX_Categories_Usr_Id",
                schema: "Register",
                table: "Categories");

            migrationBuilder.DropIndex(
                name: "IX_Camaras_Usr_Id",
                schema: "Register",
                table: "Camaras");

            migrationBuilder.DropColumn(
                name: "Usr_Id",
                schema: "Register",
                table: "XObjects");

            migrationBuilder.DropColumn(
                name: "Usr_Id",
                schema: "Register",
                table: "Persons");

            migrationBuilder.DropColumn(
                name: "Usr_Id",
                schema: "Register",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "Usr_Id",
                schema: "Register",
                table: "Camaras");
        }
    }
}

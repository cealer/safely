﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Monitor.Data.Migrations
{
    public partial class AddTimeZonetoCamara : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CamTimeZone",
                schema: "Register",
                table: "Camaras",
                type: "nvarchar(100)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CamTimeZone",
                schema: "Register",
                table: "Camaras");
        }
    }
}
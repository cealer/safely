﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Monitor.Data.Migrations
{
    public partial class AddDevice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Devices",
                schema: "Register",
                columns: table => new
                {
                    Dev_Id = table.Column<string>(type: "char(18)", nullable: false),
                    Dev_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Cam_Enabled = table.Column<bool>(nullable: false),
                    CodeId = table.Column<string>(nullable: true),
                    Dev_Description = table.Column<string>(maxLength: 20, nullable: false),
                    Usr_Id = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Devices", x => x.Dev_Id);
                    table.ForeignKey(
                        name: "FK_Devices_AspNetUsers_Usr_Id",
                        column: x => x.Usr_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Devices_Usr_Id",
                schema: "Register",
                table: "Devices",
                column: "Usr_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Devices",
                schema: "Register");
        }
    }
}

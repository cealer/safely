﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Monitor.Data.Migrations
{
    public partial class ChangenametoDevEnabledDeviceTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Cam_Enabled",
                schema: "Register",
                table: "Devices",
                newName: "Dev_Enabled");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Dev_Enabled",
                schema: "Register",
                table: "Devices",
                newName: "Cam_Enabled");
        }
    }
}

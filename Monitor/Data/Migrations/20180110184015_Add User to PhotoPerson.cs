﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Monitor.Data.Migrations
{
    public partial class AddUsertoPhotoPerson : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Usr_Id",
                schema: "Register",
                table: "PhotoPerson",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PhotoPerson_Usr_Id",
                schema: "Register",
                table: "PhotoPerson",
                column: "Usr_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PhotoPerson_AspNetUsers_Usr_Id",
                schema: "Register",
                table: "PhotoPerson",
                column: "Usr_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PhotoPerson_AspNetUsers_Usr_Id",
                schema: "Register",
                table: "PhotoPerson");

            migrationBuilder.DropIndex(
                name: "IX_PhotoPerson_Usr_Id",
                schema: "Register",
                table: "PhotoPerson");

            migrationBuilder.DropColumn(
                name: "Usr_Id",
                schema: "Register",
                table: "PhotoPerson");
        }
    }
}

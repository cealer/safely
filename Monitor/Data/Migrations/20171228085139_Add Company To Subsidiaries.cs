﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Monitor.Data.Migrations
{
    public partial class AddCompanyToSubsidiaries : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Subsidiaries_Com_Id",
                schema: "Register",
                table: "Subsidiaries",
                column: "Com_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Subsidiaries_Companies_Com_Id",
                schema: "Register",
                table: "Subsidiaries",
                column: "Com_Id",
                principalSchema: "Register",
                principalTable: "Companies",
                principalColumn: "Com_Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subsidiaries_Companies_Com_Id",
                schema: "Register",
                table: "Subsidiaries");

            migrationBuilder.DropIndex(
                name: "IX_Subsidiaries_Com_Id",
                schema: "Register",
                table: "Subsidiaries");
        }
    }
}

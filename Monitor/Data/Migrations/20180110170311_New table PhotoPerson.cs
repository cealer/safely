﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Monitor.Data.Migrations
{
    public partial class NewtablePhotoPerson : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PhotoPerson",
                schema: "Register",
                columns: table => new
                {
                    Pho_Id = table.Column<string>(type: "char(18)", nullable: false),
                    DateRegister = table.Column<DateTime>(nullable: false),
                    Enabled = table.Column<bool>(nullable: false),
                    Pho_PerId = table.Column<string>(type: "char(18)", nullable: true),
                    UrlImage = table.Column<string>(nullable: true),
                    persistedFaceIds = table.Column<string>(nullable: true),
                    personIdApi = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhotoPerson", x => x.Pho_Id);
                    table.ForeignKey(
                        name: "FK_PhotoPerson_Persons_Pho_PerId",
                        column: x => x.Pho_PerId,
                        principalSchema: "Register",
                        principalTable: "Persons",
                        principalColumn: "Per_Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PhotoPerson_Pho_PerId",
                schema: "Register",
                table: "PhotoPerson",
                column: "Pho_PerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PhotoPerson",
                schema: "Register");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Monitor.Data.Migrations
{
    public partial class adduserinAlarms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Usr_Id",
                schema: "Register",
                table: "Alarms",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Alarms_Usr_Id",
                schema: "Register",
                table: "Alarms",
                column: "Usr_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Alarms_AspNetUsers_Usr_Id",
                schema: "Register",
                table: "Alarms",
                column: "Usr_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Alarms_AspNetUsers_Usr_Id",
                schema: "Register",
                table: "Alarms");

            migrationBuilder.DropIndex(
                name: "IX_Alarms_Usr_Id",
                schema: "Register",
                table: "Alarms");

            migrationBuilder.DropColumn(
                name: "Usr_Id",
                schema: "Register",
                table: "Alarms");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Monitor.Data.Migrations
{
    public partial class AddUserToSubsidiaries : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Usr_Id",
                schema: "Register",
                table: "Subsidiaries",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Subsidiaries_Usr_Id",
                schema: "Register",
                table: "Subsidiaries",
                column: "Usr_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Subsidiaries_AspNetUsers_Usr_Id",
                schema: "Register",
                table: "Subsidiaries",
                column: "Usr_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subsidiaries_AspNetUsers_Usr_Id",
                schema: "Register",
                table: "Subsidiaries");

            migrationBuilder.DropIndex(
                name: "IX_Subsidiaries_Usr_Id",
                schema: "Register",
                table: "Subsidiaries");

            migrationBuilder.DropColumn(
                name: "Usr_Id",
                schema: "Register",
                table: "Subsidiaries");
        }
    }
}

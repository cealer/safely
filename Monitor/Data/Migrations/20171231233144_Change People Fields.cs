﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Monitor.Data.Migrations
{
    public partial class ChangePeopleFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropIndex(
            //    name: "Sub_Description_uq",
            //    schema: "Register",
            //    table: "Subsidiaries");

            //migrationBuilder.DropIndex(
            //    name: "Per_Dni_uq",
            //    schema: "Register",
            //    table: "Persons");

            //migrationBuilder.DropIndex(
            //    name: "Per_Email_uq",
            //    schema: "Register",
            //    table: "Persons");

            //migrationBuilder.DropIndex(
            //    name: "Neu_Description_uq",
            //    schema: "Register",
            //    table: "NeuralNetworks");

            //migrationBuilder.DropIndex(
            //    name: "Cam_Source_uq",
            //    schema: "Register",
            //    table: "Camaras");

            //migrationBuilder.DropIndex(
            //    name: "Alm_Description_uq",
            //    schema: "Register",
            //    table: "Alarms");

            //migrationBuilder.DropIndex(
            //    name: "Tra_Description_uq",
            //    schema: "Detection",
            //    table: "Training");

            migrationBuilder.DropColumn(
                name: "Per_Dni",
                schema: "Register",
                table: "Persons");

            migrationBuilder.AddColumn<string>(
                name: "Per_Identification",
                schema: "Register",
                table: "Persons",
                type: "nvarchar(50)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Per_Identification",
                schema: "Register",
                table: "Persons");

            migrationBuilder.AddColumn<string>(
                name: "Per_Dni",
                schema: "Register",
                table: "Persons",
                type: "char(8)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "Sub_Description_uq",
                schema: "Register",
                table: "Subsidiaries",
                column: "Sub_Description",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Per_Dni_uq",
                schema: "Register",
                table: "Persons",
                column: "Per_Dni",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Per_Email_uq",
                schema: "Register",
                table: "Persons",
                column: "Per_Email",
                unique: true,
                filter: "[Per_Email] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "Neu_Description_uq",
                schema: "Register",
                table: "NeuralNetworks",
                column: "Neu_Description",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Cam_Source_uq",
                schema: "Register",
                table: "Camaras",
                column: "Cam_Source",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Alm_Description_uq",
                schema: "Register",
                table: "Alarms",
                column: "Alm_Description",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Tra_Description_uq",
                schema: "Detection",
                table: "Training",
                column: "Tra_Description",
                unique: true);
        }
    }
}

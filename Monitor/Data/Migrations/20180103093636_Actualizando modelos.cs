﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Monitor.Data.Migrations
{
    public partial class Actualizandomodelos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropIndex(
            //    name: "Per_Path",
            //    schema: "Register",
            //    table: "Persons");

            migrationBuilder.AlterColumn<string>(
                name: "Per_Path",
                schema: "Register",
                table: "Persons",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            //migrationBuilder.CreateIndex(
            //    name: "Per_Path",
            //    schema: "Register",
            //    table: "Persons",
            //    column: "Per_Path",
            //    unique: true,
            //    filter: "[Per_Path] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "Per_Path",
                schema: "Register",
                table: "Persons");

            migrationBuilder.AlterColumn<string>(
                name: "Per_Path",
                schema: "Register",
                table: "Persons",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "Per_Path",
                schema: "Register",
                table: "Persons",
                column: "Per_Path",
                unique: true);
        }
    }
}
